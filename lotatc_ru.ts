<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="51"/>
        <source>Author: DArt</source>
        <translation>Автор: Dart</translation>
    </message>
    <message>
        <source>Thanks to all beta-testers and translators who help me</source>
        <translation type="vanished">Спасибо всем оказавшим мне помощь бета-тестерам и переводчикам</translation>
    </message>
    <message>
        <source>Thanks to my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation type="vanished">Благодарность моему скваду: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Special mention for Azrayen&apos; for the testing and documentation awesome work.</source>
        <translation type="vanished">Особая благодарность Azrayen&apos; за помошь в тестировании и работу с документацией.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="28"/>
        <source>THANKS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="37"/>
        <source>my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="40"/>
        <source>Azrayen&apos; and sp@t for the testing and documentation awesome work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="41"/>
        <source>Toubib for its work on Nevada map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="42"/>
        <source>Snoopy -76th vFS- for its charts and the work on airport views.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="43"/>
        <source>all beta-testers and translators who help me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="48"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="49"/>
        <source>Build: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="50"/>
        <source>User id: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="61"/>
        <source>Translators:</source>
        <translation>Переводчики:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="72"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="73"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
</context>
<context>
    <name>AirportInfo</name>
    <message>
        <source>Id</source>
        <translation type="vanished">Id</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="vanished">Код</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Длина</translation>
    </message>
    <message>
        <source>Frequencies</source>
        <translation type="vanished">Частоты</translation>
    </message>
    <message>
        <source>TACAN</source>
        <translation type="vanished">TACAN</translation>
    </message>
    <message>
        <source>ILS available on %1MHz</source>
        <translation type="vanished">ILS работает на %1MHz </translation>
    </message>
    <message>
        <source>ILS available</source>
        <translation type="vanished">Доступно ILS</translation>
    </message>
    <message>
        <source>No ILS</source>
        <translation type="vanished">Без ILS </translation>
    </message>
    <message>
        <source>Current controllers</source>
        <translation type="vanished">Контроль </translation>
    </message>
    <message>
        <source>Take approach</source>
        <translation type="vanished">Подход</translation>
    </message>
    <message>
        <source>No control available</source>
        <translation type="vanished">Без контроля</translation>
    </message>
</context>
<context>
    <name>AirportProperties</name>
    <message>
        <source>Id</source>
        <translation type="obsolete">Id</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Код</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="obsolete">Позиция</translation>
    </message>
    <message>
        <source>Altitude</source>
        <translation type="obsolete">Высота</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="obsolete">Длина</translation>
    </message>
    <message>
        <source>Heading</source>
        <translation type="obsolete">Направление</translation>
    </message>
    <message>
        <source>Frequencies</source>
        <translation type="obsolete">Частоты</translation>
    </message>
    <message>
        <source>TACAN</source>
        <translation type="obsolete">TACAN</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="107"/>
        <source>Open a chart view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="119"/>
        <source>METAR : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="129"/>
        <source>Create ATIS on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Take approach</source>
        <translation type="unfinished">Подход</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Release approach</source>
        <translation type="unfinished">Убрать окно</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="223"/>
        <source>No control available</source>
        <translation type="unfinished">Без контроля</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="236"/>
        <source>ILS available on %1MHz</source>
        <translation type="unfinished">ILS работает на %1MHz </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="239"/>
        <source>No ILS</source>
        <translation type="unfinished">Без ILS </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="242"/>
        <source>Current controllers</source>
        <translation type="unfinished">Контроль </translation>
    </message>
</context>
<context>
    <name>AirportPropertiesModel</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="27"/>
        <source>Id</source>
        <translation type="unfinished">Id</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="28"/>
        <source>Code</source>
        <translation type="unfinished">Код</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="29"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="30"/>
        <source>Position</source>
        <translation type="unfinished">Позиция</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="31"/>
        <source>Altitude</source>
        <translation type="unfinished">Высота</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="32"/>
        <source>QFE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="33"/>
        <source>QNH</source>
        <translation type="unfinished">QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="34"/>
        <source>Declinaison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="obsolete">Длина</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="35"/>
        <source>Heading</source>
        <translation type="unfinished">Направление</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="36"/>
        <source>Frequencies</source>
        <translation type="unfinished">Частоты</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="38"/>
        <source>TACAN</source>
        <translation type="unfinished">TACAN</translation>
    </message>
</context>
<context>
    <name>BlockType</name>
    <message>
        <location filename="../lotatc/client/block/BlockType.cpp" line="19"/>
        <source>UNK</source>
        <comment>Short version of unknow</comment>
        <translation>Не определен</translation>
    </message>
</context>
<context>
    <name>Braa</name>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="88"/>
        <source>H</source>
        <comment>Hot ex: 45/23Nm/1200ft/H</comment>
        <translation>Выше</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="89"/>
        <source>FR</source>
        <comment>Flanking Right ex: 45/23Nm/1200ft/FR</comment>
        <translation>Справа</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="90"/>
        <source>C</source>
        <comment>Cold ex: 45/23Nm/1200ft/C</comment>
        <translation>Ниже</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="91"/>
        <source>FL</source>
        <comment>Flanking Left ex: 45/23Nm/1200ft/FL</comment>
        <translation>Слева</translation>
    </message>
</context>
<context>
    <name>BullseyeProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="45"/>
        <source>Position</source>
        <translation type="unfinished">Позиция</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="62"/>
        <source>Bullseye properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="74"/>
        <source>Set coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="83"/>
        <source>Restore from mission</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CarrierProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="38"/>
        <source>Ship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="39"/>
        <source>Airport</source>
        <translation type="unfinished">Аэродром</translation>
    </message>
</context>
<context>
    <name>ChatManager</name>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="18"/>
        <source>My side</source>
        <translation>Моя сторона</translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="22"/>
        <source>All</source>
        <translation>Всем</translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="14"/>
        <source>LotAtc only</source>
        <translation>Только LotAtc</translation>
    </message>
    <message>
        <source>Internal</source>
        <translation type="obsolete">Внутренний</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ChatMessage.qml" line="57"/>
        <source>Me:</source>
        <translation>Я:</translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/ColorButton.qml" line="95"/>
        <source>Click to select, Shift+click to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="180"/>
        <source>Address:</source>
        <translation>Адрес:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="188"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="198"/>
        <source>Coalition:</source>
        <translation>Каолиция:</translation>
    </message>
    <message>
        <source>blue</source>
        <translation type="obsolete">Синяя</translation>
    </message>
    <message>
        <source>red</source>
        <translation type="obsolete">Красная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="210"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="220"/>
        <source>Pseudo:</source>
        <translation>Никнейм:</translation>
    </message>
    <message>
        <source>Bookmark:</source>
        <translation type="vanished">Закладка:</translation>
    </message>
    <message>
        <source>Bookmark current</source>
        <translation type="vanished">Текущая закладка</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="167"/>
        <source>Name:</source>
        <translation type="unfinished">Имя юнита:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="146"/>
        <source>Delete current bookmark</source>
        <translation>Удалить закладку</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="129"/>
        <source>New bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="136"/>
        <source>Save current bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="160"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="171"/>
        <source>Enter a name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Connect</source>
        <translation>Соединение</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Waiting maps...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="245"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>CoordinatesDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="56"/>
        <source>Enter coordinates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="63"/>
        <source>Format:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Restore</source>
        <comment>Restore the window</comment>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Maximize</source>
        <comment>Maximize the window</comment>
        <translation>Максимально</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="67"/>
        <source>Minimize</source>
        <comment>Minimize the window</comment>
        <translation>Минимально</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="72"/>
        <source>Make tab</source>
        <translation>В закладки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="81"/>
        <source>Make dialog</source>
        <translation>В центр</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="89"/>
        <source>Make dock</source>
        <translation>В исходную</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="97"/>
        <source>Opacity</source>
        <translation>Прозрачность</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="118"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpDlg</name>
    <message>
        <source>Bookmarks</source>
        <translation type="vanished">Закладки</translation>
    </message>
</context>
<context>
    <name>LabelEdit</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="25"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="37"/>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="56"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="44"/>
        <source>Hovered</source>
        <translation>Расширенный</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="60"/>
        <source>Connect</source>
        <translation>Соединение</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="63"/>
        <source>Connect to a server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="68"/>
        <source>Disconnect</source>
        <translation>Отсоединиться</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="71"/>
        <source>Disconnect current session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="79"/>
        <source>Open options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="86"/>
        <source>New version available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="94"/>
        <source>Display online help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="100"/>
        <source>Information on this software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="108"/>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="115"/>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="121"/>
        <source>Enter/Exit fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="127"/>
        <source>On top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="129"/>
        <source>Activate on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="138"/>
        <source>Lock map scale/move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="147"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="194"/>
        <source>Click to open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="232"/>
        <source>Change bearing mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="238"/>
        <source>Change units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="276"/>
        <source>Mission time, show/hide seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="288"/>
        <source>Show controllers informations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="296"/>
        <source>Show server settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Опции</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="83"/>
        <source>News</source>
        <translation>Новости</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Помощь</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>METRIC</source>
        <translation>Метрическая</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>IMPERIAL</source>
        <translation>Английская</translation>
    </message>
</context>
<context>
    <name>MapItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItem.qml" line="224"/>
        <source>%1</source>
        <translatorcomment>Makes no sense to translate</translatorcomment>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>MapItemProperties</name>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Неизвестно</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="75"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="76"/>
        <source>TN: %1</source>
        <comment>Track Number of the unit</comment>
        <translation type="unfinished">Номер юнита: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="91"/>
        <source>Group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="93"/>
        <source>Transponder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="94"/>
        <source>Altitude</source>
        <translation type="unfinished">Высота</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="95"/>
        <source>Heading</source>
        <translation type="unfinished">Направление</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="96"/>
        <source>Ground Speed</source>
        <translation type="unfinished">Скорость у земли</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="98"/>
        <source>Vertical Indicator</source>
        <translation type="unfinished">Вертикальная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="99"/>
        <source>Position</source>
        <translation type="unfinished">Позиция</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="100"/>
        <source>BullsEye</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="104"/>
        <source>Detection range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="106"/>
        <source>Threat range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="139"/>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation type="unfinished">Имя юнита:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="152"/>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="245"/>
        <source>Apply</source>
        <translation type="unfinished">Применить</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="171"/>
        <source>Side:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="198"/>
        <source>Symbol:</source>
        <translation type="unfinished">Символ:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="231"/>
        <source>Comment:</source>
        <comment>Comment on the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="272"/>
        <source>Message:</source>
        <translation type="unfinished">Сообщение:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="285"/>
        <source>Text to send...</source>
        <translation type="unfinished">Cообщение...</translation>
    </message>
    <message>
        <source>Show on map</source>
        <translation type="obsolete">Показать на карте</translation>
    </message>
</context>
<context>
    <name>MyDockDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/MyDockDialog.qml" line="62"/>
        <source>Waiting connection...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyModels</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="48"/>
        <source>Off</source>
        <translation>Выкл</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="49"/>
        <source>On</source>
        <translation>Вкл</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="55"/>
        <source>Hide</source>
        <translation>Скрыть</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="56"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="68"/>
        <source>Neutral</source>
        <translation type="unfinished">Нейтральные</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="80"/>
        <source>Blue</source>
        <translation>Синий</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="81"/>
        <source>Red</source>
        <translation>Красный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="123"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="124"/>
        <source>Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="125"/>
        <source>Enemy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="140"/>
        <source>Light</source>
        <translation>Светлый</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="141"/>
        <source>Dark</source>
        <translation>Темный</translation>
    </message>
    <message>
        <source>Very Dark</source>
        <translation type="vanished">Очень темный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="183"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="184"/>
        <source>French</source>
        <translation>Французский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="185"/>
        <source>Deutsch</source>
        <translation>Немецкий</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="186"/>
        <source>Italian</source>
        <translation>Итальянский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="187"/>
        <source>Polish</source>
        <translation>Польский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="188"/>
        <source>Czech</source>
        <translation>Чешский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="189"/>
        <source>Portuguese</source>
        <translation>Португальский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="190"/>
        <source>Serbian</source>
        <translation>Сербский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="191"/>
        <source>Spanish</source>
        <translation>Испанский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="192"/>
        <source>Chinese</source>
        <translation>Китайский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="193"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="194"/>
        <source>Swedish</source>
        <translation>Шведский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="195"/>
        <source>Brazilian Portuguese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="210"/>
        <source>Solid</source>
        <comment>Solid pattern brush</comment>
        <translation>Сплошная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="211"/>
        <source>Dense</source>
        <comment>Dense pattern brush</comment>
        <translation>Твердая</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="212"/>
        <source>Horizontal</source>
        <comment>Horizontal pattern brush</comment>
        <translation>Горизонтальная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="213"/>
        <source>Vertical</source>
        <comment>Horizontal pattern brush</comment>
        <translation>Вертикальная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="214"/>
        <source>Diagonal</source>
        <comment>Diagonal pattern brush</comment>
        <translation>Наклонная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="215"/>
        <source>Cross</source>
        <comment>Cross pattern brush</comment>
        <translation>Крест</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="223"/>
        <source>Unit Name (or TN)</source>
        <translation>Имя юнита (или TN)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="224"/>
        <source>%(name|10) limit name to 10 characters</source>
        <translation>%(имя|10) ограничение имени в 10 знаков</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="228"/>
        <source>Group Name (if available)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="229"/>
        <source>%(group_name|10) limit name to 10 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="233"/>
        <source>Unit type</source>
        <translation>Тип юнита</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="238"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="239"/>
        <source>%(comment|10) limit comment to 10 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="243"/>
        <source>Unit heading</source>
        <translation>Напрвление юнита</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="244"/>
        <source>Use %(heading|option) with option=true/mag/truemag for true/magnetic deviation correction (default is auto)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="273"/>
        <source>Ground speed in Mach format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="288"/>
        <source>Transponder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="301"/>
        <source>Longitude/Latitude Decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="309"/>
        <source>HF</source>
        <comment>HF frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="311"/>
        <source>VHF</source>
        <comment>VHF frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="370"/>
        <source>Low (4x)</source>
        <comment>antialiasing to 4x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="371"/>
        <source>Mid (8x)</source>
        <comment>antialiasing to 8x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="372"/>
        <source>High (16x)</source>
        <comment>antialiasing to 16x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="428"/>
        <source>Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="430"/>
        <source>Show circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="431"/>
        <source>Change unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="432"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="433"/>
        <source>Use bullseye as reference for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>True</source>
        <comment>True bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>T</source>
        <comment>Shortcut for True bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>Magnetic</source>
        <comment>Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>M</source>
        <comment>Shortcut for Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>True/Magnetic</source>
        <comment>True/Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>T+M</source>
        <comment>Shortcut for True+Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="475"/>
        <source>Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="476"/>
        <source>Helicopter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="477"/>
        <source>Ground moving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="478"/>
        <source>Ground standing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="479"/>
        <source>Ship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="480"/>
        <source>Sam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="481"/>
        <source>Tank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="497"/>
        <source>Normal - Small font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="498"/>
        <source>Normal</source>
        <translation type="unfinished">Нормальный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="499"/>
        <source>Normal - Large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="500"/>
        <source>Normal - Very large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="501"/>
        <source>Dense - Small font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="502"/>
        <source>Dense</source>
        <translation type="unfinished">Твердая</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="503"/>
        <source>Dense - Large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="504"/>
        <source>Dense - Very large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use %(heading|mag) for magnetic deviation correction</source>
        <translation type="vanished">Использовать %(направление|mag) для корректировки магнитного склонения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="248"/>
        <source>Altitude long format</source>
        <translation>Высота - полный формат</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="253"/>
        <source>Altitude short format</source>
        <translation>Высота - сокращенный формат</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="254"/>
        <source>%(alti_short|thousand) in imperial, show altitude in thousand feets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="258"/>
        <source>Ground speed</source>
        <translation>Наземная скорость</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="263"/>
        <source>Ground speed in short format</source>
        <translation>Наземная скорость - коротко</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="268"/>
        <source>Ground speed unit</source>
        <translation>Наземная скорость - полно</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="278"/>
        <source>Position to BullsEye</source>
        <translation>Позиция к BullsEye </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="283"/>
        <source>Vertical indicator</source>
        <translation>Вертикальная</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="284"/>
        <source>%(vert_indic|nostable) to avoid stable cursor, %(vert_indic|n) with n=0/1/2 for different arrows</source>
        <translation>%(vert_indic|nostable) для избежания нестабильности курсора, %(vert_indic|n) где n=0/1/2 для разограничения стрелок указателя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="299"/>
        <source>Decimal</source>
        <translation>Разделитель (десятичный)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="300"/>
        <source>Longitude/Latitude</source>
        <translation>Долгота/Широта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="302"/>
        <source>MGRS</source>
        <translation>Координаты MGRS</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="303"/>
        <source>UTM</source>
        <translation>Координаты UTM</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="317"/>
        <source>AM</source>
        <comment>AM frequency band for radio</comment>
        <translation>АМ модул.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="310"/>
        <source>UHF</source>
        <comment>UHF frequency band for radio</comment>
        <translation>UHF - ДМВ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="142"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="318"/>
        <source>FM</source>
        <comment>FM frequency band for radio</comment>
        <translation>FM - УКВ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="333"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="334"/>
        <source>OpenGL</source>
        <translation>OpenGL</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="335"/>
        <source>DirectX</source>
        <translation>DirectX</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="351"/>
        <source>Low</source>
        <translation>Низко</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="352"/>
        <source>Mid</source>
        <translation>Средне</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="353"/>
        <source>High</source>
        <translation>Высоко</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="369"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Домой</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="389"/>
        <source>Labels</source>
        <translation>Метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="390"/>
        <source>Shortcuts</source>
        <translation>Сокращения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="391"/>
        <source>Detection profiles</source>
        <translation>Определить профиль</translation>
    </message>
    <message>
        <source>Client FAQ</source>
        <translation type="vanished">FAQ клиента</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="429"/>
        <source>Show background label</source>
        <translation type="unfinished">Показать фоновые метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="482"/>
        <source>Airport</source>
        <translation type="unfinished">Аэродром</translation>
    </message>
</context>
<context>
    <name>MyWebView</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>NetDrawingText</name>
    <message>
        <location filename="../lotatc/common/NetDrawingText.cpp" line="55"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>NetObject</name>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="161"/>
        <source>You try to connect to an older incompatible version, server should be upgraded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="163"/>
        <source>You try to connect to an newer incompatible version, client should be upgraded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewsDlg</name>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Загрузка...</translation>
    </message>
    <message>
        <source>News</source>
        <translation type="vanished">Новости</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ошибка</translation>
    </message>
    <message>
        <source>See on LotAtc website: </source>
        <translation type="obsolete">Смотрите на сайте LotAtc: </translation>
    </message>
</context>
<context>
    <name>OptionsApproach</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="15"/>
        <source>Number of contact updates to display on glide</source>
        <translation>Число обновлений для отображения на глиссаде</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="32"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
</context>
<context>
    <name>OptionsChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="20"/>
        <source>Play sound on new messages</source>
        <translation type="unfinished">Звук при новом сообщении</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="26"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="30"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="35"/>
        <source>Please choose a color for background chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="39"/>
        <source>Foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="44"/>
        <source>Please choose a color for foreground chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="15"/>
        <source>General</source>
        <translation>Главное</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="16"/>
        <source>Items</source>
        <translation>Элементы</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="17"/>
        <source>Map</source>
        <translation type="unfinished">Карта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="18"/>
        <source>Labels</source>
        <translation>Метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="19"/>
        <source>Approach</source>
        <translation>Подход</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="20"/>
        <source>Shortcuts</source>
        <translation type="unfinished">Сокращения</translation>
    </message>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="obsolete">Выбор языка (требуется перезагрузка приложения):</translation>
    </message>
    <message>
        <source>Choose a skin</source>
        <translation type="obsolete">Поменять скин</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="obsolete">Выбор схемы приложения (потребуется перезагрузка приложения):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="21"/>
        <source>Chat</source>
        <translation type="unfinished">Чат</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="obsolete">Звук при новом сообщении</translation>
    </message>
    <message>
        <source>Choose symbology</source>
        <translation type="obsolete">Выбрать символ</translation>
    </message>
    <message>
        <source>Define color for friends</source>
        <translation type="obsolete">Определить цвет для друзей</translation>
    </message>
    <message>
        <source>Choose color</source>
        <translation type="obsolete">Выбор цвета</translation>
    </message>
    <message>
        <source>Please choose a color for friend unit</source>
        <translation type="obsolete">Цвет дружественного юнита</translation>
    </message>
    <message>
        <source>Default color</source>
        <translation type="obsolete">Цвет по умолчанию</translation>
    </message>
    <message>
        <source>Define color for enemies</source>
        <translation type="obsolete">Определение цвета врагов</translation>
    </message>
    <message>
        <source>Please choose a color for enemies unit</source>
        <translation type="obsolete">Цвет вражеского юнита</translation>
    </message>
    <message>
        <source>Define size of Item</source>
        <translation type="obsolete">Определение размера</translation>
    </message>
    <message>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation type="obsolete">Определение вектора скорости (назначить в 0 для фиксирования скорости)</translation>
    </message>
    <message>
        <source>Zoom independent</source>
        <translation type="obsolete">Независимый зум</translation>
    </message>
    <message>
        <source>%1 seconds</source>
        <translation type="obsolete">%1 секунд</translation>
    </message>
    <message>
        <source>Define labels for friends</source>
        <translation type="obsolete">Определение меток друзей</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="obsolete">Определение меток врагов</translation>
    </message>
    <message>
        <source>Show background for label (Key: Alt+L)</source>
        <translation type="obsolete">Отобразить тень для меток l (Key: Alt+L)</translation>
    </message>
    <message>
        <source>Size of font label:</source>
        <translation type="obsolete">Размер метки:</translation>
    </message>
    <message>
        <source>Available values:</source>
        <translation type="obsolete">Доступные значения:</translation>
    </message>
    <message>
        <source>Unit Name (or TN)</source>
        <translation type="obsolete">Имя юнита (или TN)</translation>
    </message>
    <message>
        <source>Unit type</source>
        <translation type="obsolete">Тип юнита</translation>
    </message>
    <message>
        <source>Altitude long format</source>
        <translation type="obsolete">Высота - полный формат</translation>
    </message>
    <message>
        <source>Altitude short format</source>
        <translation type="obsolete">Высота - сокращенный формат</translation>
    </message>
    <message>
        <source>Ground speed</source>
        <translation type="obsolete">Наземная скорость</translation>
    </message>
    <message>
        <source>Ground speed in short format</source>
        <translation type="obsolete">Наземная скорость - коротко</translation>
    </message>
    <message>
        <source>Ground speed unit</source>
        <translation type="obsolete">Наземная скорость - полно</translation>
    </message>
    <message>
        <source>Position to BullsEye</source>
        <translation type="obsolete">Позиция к BullsEye </translation>
    </message>
    <message>
        <source>Vertical indicator</source>
        <translation type="obsolete">Вертикальная</translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="obsolete">Жми сюда для получения полной документации</translation>
    </message>
</context>
<context>
    <name>OptionsGeneral</name>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="vanished">Выбор языка (требуется перезагрузка приложения):</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="vanished">Выбор схемы приложения (потребуется перезагрузка приложения):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="27"/>
        <source>Choose a language:</source>
        <translation>Выбор языка:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="44"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="68"/>
        <source>Choose a variant for the UI (size of the UI):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="83"/>
        <source>Choose a theme for the application:</source>
        <translation>Выбор темы приложения:</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="vanished">Звук при новом сообщении</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="134"/>
        <source>Performances</source>
        <translation>Производительность</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="140"/>
        <source>Graphical effects:</source>
        <translation>Эффекты графики:</translation>
    </message>
    <message>
        <source>Change graphic engine [current=%1]:</source>
        <comment>The %1 will be replacing by current engine: auto,opengl, directx...</comment>
        <translation type="vanished">Выбор графического движка [текущий=%1]:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="160"/>
        <source>Antialiasing:</source>
        <translation>Антиальясинг:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="113"/>
        <source>Restore default dock and windows positions</source>
        <translation>Восстановить позиции окон по умолчанию</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="98"/>
        <source>Choose a variation color for the application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="127"/>
        <source>Check update at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="172"/>
        <source>(*) : Need application restart</source>
        <translation>(*) : Требуется перезапуск приложения</translation>
    </message>
    <message>
        <source>Restore default dock and windows positions (need application restart)</source>
        <translation type="vanished">Восстановить позиций окон по умолчанию (требуется перезагрузка приложения)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="115"/>
        <source>Click to reset</source>
        <translation>Сброс</translation>
    </message>
</context>
<context>
    <name>OptionsItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="66"/>
        <source>Choose symbology</source>
        <translation>Выбрать символ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="27"/>
        <source>Define color for items</source>
        <translation>Цвет элементов</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="32"/>
        <source>Friends</source>
        <translation>Друзья</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="36"/>
        <source>Please choose a color for friends unit</source>
        <translation>Цвет дружественных юнитов</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="41"/>
        <source>Enemies</source>
        <translation>Враги</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="45"/>
        <source>Please choose a color for enemies unit</source>
        <translation>Цвет вражеского юнита</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="49"/>
        <source>Neutral</source>
        <translation>Нейтральные</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="53"/>
        <source>Please choose a color for neutral unit</source>
        <translation>Цвет нейтральных юнитов</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="62"/>
        <source>Items parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="77"/>
        <source>Define size of Item</source>
        <translation>Определение размера</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="127"/>
        <source>Define opacity of circles</source>
        <translation>Прозрачность круга</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="132"/>
        <source>Detection:</source>
        <translation>Определён:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="147"/>
        <source>Threat:</source>
        <translation>Угроза:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="90"/>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation>Определение вектора скорости (назначить в 0 для фиксирования скорости)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>Zoom independent</source>
        <translation>Независимый зум</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>%1 seconds</source>
        <translation>%1 секунд</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="111"/>
        <source>Define number of ghost</source>
        <translation>Определить число отметок</translation>
    </message>
</context>
<context>
    <name>OptionsLabel</name>
    <message>
        <source>Define labels for friends</source>
        <translation type="vanished">Определение меток друзей</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="vanished">Определение меток врагов</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="19"/>
        <source>Parameters</source>
        <translation type="unfinished">Параметры</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="23"/>
        <source>Show background for label (Key: Alt+L)</source>
        <translation>Отобразить тень для меток (Key: Alt+L)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="29"/>
        <source>Size of font label:</source>
        <translation>Размер метки:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="40"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="44"/>
        <source>Please choose a color for label background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="48"/>
        <source>Foreground:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="52"/>
        <source>Please choose a color for label foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="56"/>
        <source>Background mode alert 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="60"/>
        <source>Please choose a color for label background in simple alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="64"/>
        <source>Background mode alert 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="68"/>
        <source>Please choose a color for label background in alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="76"/>
        <source>Define contents for labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="79"/>
        <source>Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="80"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="138"/>
        <source>Full documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enemies</source>
        <translation type="obsolete">Враги</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="126"/>
        <source>Available values:</source>
        <translation>Доступные значения:</translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="vanished">Нажать сюда для получения полной документации</translation>
    </message>
</context>
<context>
    <name>OptionsMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="20"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="24"/>
        <source>Land</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="29"/>
        <source>Please choose a color for land</source>
        <translation type="unfinished">Выбор цвета земли</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="33"/>
        <source>Sea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="38"/>
        <source>Please choose a color for sea</source>
        <translation type="unfinished">Выбор цвета моря</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="42"/>
        <source>Range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="54"/>
        <source>Please choose a color for range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="58"/>
        <source>Measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="63"/>
        <source>Please choose a color for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="67"/>
        <source>Measure line background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="72"/>
        <source>Please choose a color for background measure line label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="76"/>
        <source>BRAA line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="81"/>
        <source>Please choose a color for braa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="88"/>
        <source>Advanced</source>
        <translation type="unfinished">Продвинутый</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="94"/>
        <source>Brightness</source>
        <translation type="unfinished">Яркость</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="112"/>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="135"/>
        <source>Default</source>
        <translation type="unfinished">По умолчанию</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="116"/>
        <source>Contrast</source>
        <translation type="unfinished">Контрастность</translation>
    </message>
</context>
<context>
    <name>OptionsShortcut</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="30"/>
        <source>You can change shortcuts here, just double-clic on it to change it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="61"/>
        <source>Click on a shortcut to edit it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="70"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="75"/>
        <source>Restore</source>
        <translation type="unfinished">Восстановить</translation>
    </message>
</context>
<context>
    <name>PageAirportView</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageAirportView.qml" line="12"/>
        <source>Airport view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageBraa</name>
    <message>
        <source>Show BRAA on map:</source>
        <translation type="vanished">Показать BRAA на карте:</translation>
    </message>
    <message>
        <source>Show BRAA text:</source>
        <translation type="vanished">Показать текст BRAA:</translation>
    </message>
    <message>
        <source>List of all BRAA in use</source>
        <translation type="vanished">Список используемых BRAA</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="obsolete">Исходный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="52"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="90"/>
        <source>SendIt</source>
        <translation>Послать это</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="170"/>
        <source>Unit</source>
        <translation>Юнит</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="145"/>
        <source>Target</source>
        <translation>Цель</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="13"/>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="146"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="24"/>
        <source>list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="25"/>
        <source>Options</source>
        <translation type="unfinished">Опции</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="144"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="149"/>
        <source>Interception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>METRIC</source>
        <translation>Метрическая</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>IMPERIAL</source>
        <translation>Английская</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="186"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="216"/>
        <source>Delete selected</source>
        <translation>Удалить выбранное</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="234"/>
        <source>Delete all</source>
        <translation>Удалить все</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="253"/>
        <source>Show BRAA on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="259"/>
        <source>Show BRAA text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="266"/>
        <source>Show BRAA interception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="274"/>
        <source>Send to pilot with unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="279"/>
        <source>Send to pilot every:</source>
        <translation>Послать каждому пилоту:</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Удалить</translation>
    </message>
</context>
<context>
    <name>PageChart</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChart.qml" line="14"/>
        <source>Chart view for %1</source>
        <comment>%1 replaced by airport name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="12"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <source>Current channel:</source>
        <translation type="vanished">Текущий канал:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="64"/>
        <source>Text to send...</source>
        <translation>Cообщение...</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="obsolete">Отослать</translation>
    </message>
</context>
<context>
    <name>PageDraw</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="14"/>
        <source>Draw</source>
        <translation>Рисовать</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="95"/>
        <source>Please choose a font</source>
        <translation>Выбрать шрифт</translation>
    </message>
    <message>
        <source>Show draw</source>
        <translation type="vanished">Показать рисунок</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="169"/>
        <source>Edit Mode</source>
        <translation>Редактирвание</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="185"/>
        <source>Actions</source>
        <translation>Действие</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="254"/>
        <source>Delete All</source>
        <translation>Удалить все</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="256"/>
        <source>All private draws</source>
        <translation>Все свои рисунки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="260"/>
        <source>All shared draws (WARNING)</source>
        <translation>Все общие рисунки (Осторожно!)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="268"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="195"/>
        <source>New draw</source>
        <translation>Новый рисунок</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="110"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="126"/>
        <source>Please choose a file</source>
        <translation>Выбрать файл</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="177"/>
        <source>Add text on coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="210"/>
        <source>Shared</source>
        <translation>Общий</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="212"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="235"/>
        <source>Circle</source>
        <translation>Окружность</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="217"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="239"/>
        <source>Line</source>
        <translation>Линия</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="222"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="243"/>
        <source>Polygon</source>
        <translation>Полигон</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="227"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="247"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="233"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="365"/>
        <source>Private</source>
        <translation>Свой</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="279"/>
        <source>Save to file...</source>
        <translation>Сохранить в файл...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="283"/>
        <source>Load from file...</source>
        <translation>Загрузить из файла...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="292"/>
        <source>Select a draw:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="336"/>
        <source>Parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="344"/>
        <source>Name:</source>
        <translation type="unfinished">Имя юнита:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="349"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="357"/>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="373"/>
        <source>Make it public:</source>
        <translation>Опубликовать:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="386"/>
        <source>Share it</source>
        <translation>Сделать общим</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="394"/>
        <source>Text:</source>
        <translation>Текст:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="400"/>
        <source>Enter text to display</source>
        <translation>Ввести текст для отображения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="410"/>
        <source>Font:</source>
        <translation>Шрифт:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="416"/>
        <source>Choose font</source>
        <translation>Выбор шрифта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="426"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="431"/>
        <source>Foreground color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="442"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="448"/>
        <source>Background color</source>
        <translation>Цвет подложки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="458"/>
        <source>Line width</source>
        <translation>Ширина линии</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="479"/>
        <source>Style</source>
        <translation>Стиль кисти</translation>
    </message>
</context>
<context>
    <name>PageGraph</name>
    <message>
        <source>Glide %1</source>
        <translation type="vanished">Глиссада %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="228"/>
        <source>Please choose a file</source>
        <translation>Выбрать файл</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="25"/>
        <source>Actions</source>
        <translation>Действие</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="669"/>
        <source>Release approach</source>
        <translation>Убрать окно</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="27"/>
        <source>Save graph</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="18"/>
        <source>Approach %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="294"/>
        <source>Glidepath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="496"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="555"/>
        <source>Items</source>
        <translation type="unfinished">Элементы</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="556"/>
        <source>Airport</source>
        <translation type="unfinished">Аэродром</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="557"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="689"/>
        <source>Show labels</source>
        <translation>Показать метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="697"/>
        <source>Show circles on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="708"/>
        <source>Steady level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="726"/>
        <source>Minima:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="744"/>
        <source>Glide angle:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="774"/>
        <source>LSLLC:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="591"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="640"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="649"/>
        <source>Distance(%1)</source>
        <comment>%1 will be replaced by unit: km</comment>
        <translation>Расстояние(%1)</translation>
    </message>
    <message>
        <source>Glide</source>
        <translation type="obsolete">Глиссада</translation>
    </message>
    <message>
        <source>Glide slope</source>
        <translation type="vanished">Глиссада</translation>
    </message>
    <message>
        <source>Localizer</source>
        <translation type="vanished">КРМ (курсовой радиомаяк)</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Удалить</translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="vanished">Горизонт</translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="vanished">Высота</translation>
    </message>
</context>
<context>
    <name>PageMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="51"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <source>Choose an unit (Key: Alt+N)</source>
        <comment>measurment: imperial or metric</comment>
        <translation type="obsolete">Выбор системы измерения (Key: Alt+N)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="50"/>
        <source>Filters</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="obsolete">Продвинутый</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="114"/>
        <source>Altitude filter</source>
        <translation>Фильтр по высоте</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="153"/>
        <source>Speed filter</source>
        <translation>Фильтр по скорости</translation>
    </message>
    <message>
        <source>Layers:</source>
        <translation type="vanished">Слои:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="77"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="100"/>
        <source>Please choose a color for layer</source>
        <translation>Выбор цвета слоя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="191"/>
        <source>Hide ground units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="215"/>
        <source>Show draw</source>
        <translation type="unfinished">Показать рисунок</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="221"/>
        <source>Show measure line value on bottom of the window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="237"/>
        <source>Coordinates format</source>
        <translation>Формат координат</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="251"/>
        <source>Example: %1</source>
        <translation>Пример: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="257"/>
        <source>Show airport code instead of name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Land color</source>
        <translation type="vanished">Цвет земли</translation>
    </message>
    <message>
        <source>Please choose a color for land</source>
        <translation type="vanished">Выбор цвета земли</translation>
    </message>
    <message>
        <source>Sea color</source>
        <translation type="vanished">Цвет моря</translation>
    </message>
    <message>
        <source>Please choose a color for sea</source>
        <translation type="vanished">Выбор цвета моря</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="14"/>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="49"/>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="63"/>
        <source>Choose a map:</source>
        <translation type="unfinished">Выбор карты:</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="vanished">Яркость</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">По умолчанию</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="vanished">Контрастность</translation>
    </message>
</context>
<context>
    <name>PageObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="15"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="34"/>
        <source>Enter text to filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="52"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="69"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="91"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageProperties</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Неизвестно</translation>
    </message>
    <message>
        <source>Id: %1</source>
        <comment>Id of the unit</comment>
        <translation type="obsolete">Id: %1</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Свойства</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Properties %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Selection&apos;s properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="24"/>
        <source>Actions</source>
        <translation type="unfinished">Действие</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="26"/>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="120"/>
        <source>Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="34"/>
        <source>Make this object as new bullseye reference?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="35"/>
        <source>If yes, bullseye will be set to this object (only for you)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="98"/>
        <source>Select an item/airport to see its properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Set current virtual position at this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Need radio enabled and an active radar unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="148"/>
        <source>Display range rings at this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="156"/>
        <source>Set this object as bullseye reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TN: %1</source>
        <comment>Track Number of the unit</comment>
        <translation type="vanished">Номер юнита: %1</translation>
    </message>
    <message>
        <source>Altitude</source>
        <translation type="vanished">Высота</translation>
    </message>
    <message>
        <source>Heading</source>
        <translation type="vanished">Направление</translation>
    </message>
    <message>
        <source>Ground Speed</source>
        <translation type="vanished">Скорость у земли</translation>
    </message>
    <message>
        <source>Vertical Indicator</source>
        <translation type="vanished">Вертикальная</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Позиция</translation>
    </message>
    <message>
        <source>Select a contact to see its properties</source>
        <translation type="vanished">Выбор контакта для просмотра свойств</translation>
    </message>
    <message>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation type="vanished">Имя юнита:</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Применить</translation>
    </message>
    <message>
        <source>Symbol:</source>
        <translation type="vanished">Символ:</translation>
    </message>
    <message>
        <source>Message:</source>
        <translation type="vanished">Сообщение:</translation>
    </message>
    <message>
        <source>Text to send...</source>
        <translation type="vanished">Cообщение...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="125"/>
        <source>Show on map</source>
        <translation>Показать на карте</translation>
    </message>
</context>
<context>
    <name>PageRadio</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="12"/>
        <source>Radio</source>
        <translation>Радио</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="27"/>
        <source>Airport</source>
        <translation type="unfinished">Аэродром</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="40"/>
        <source>Radar</source>
        <translation type="unfinished">Радар</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="109"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>Current Location:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>NOT DEFINED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="167"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="168"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="174"/>
        <source>Refresh list (not automatic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="179"/>
        <source>Set my position to selected one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>currently only support &lt;a href=&apos;http://tacnoworld.fr/UniversRadio/&apos;&gt;Universal Radio project&lt;/a&gt;&lt;br /&gt;This project is currently in Beta phase and made by Tacno from &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;.</source>
        <translation type="vanished">Поддержка только на  &lt;a href=&apos;http://tacnoworld.fr/UniversRadio/&apos;&gt;,проект &quot;Universal Radio&quot; &lt;/a&gt;&lt;br /&gt;Этот проект находится на стадии &quot;Бетта&quot;, изготавливается Tacno с &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;&quot;3rd-wing&quot;&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="75"/>
        <source>Radio enabled</source>
        <translation>Включить радио</translation>
    </message>
</context>
<context>
    <name>PageWeather</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="11"/>
        <source>Weather</source>
        <translation>Погода</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="142"/>
        <source>METAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="144"/>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="145"/>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="148"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="176"/>
        <source>Sky clear</source>
        <translation>Небо чистое</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="177"/>
        <source>Some clouds</source>
        <translation>Незначительная облачность</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="178"/>
        <source>Clouds</source>
        <translation>Облачно</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="179"/>
        <source>Overcast</source>
        <translation>Сплошная облачность</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="180"/>
        <source>Rain</source>
        <translation>Дождь</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="181"/>
        <source>Snow</source>
        <translation>Снег</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="182"/>
        <source>Thunderstorms</source>
        <translation>Гроза</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="183"/>
        <source>Snow thunderstorms</source>
        <translation>Метель</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="233"/>
        <source>From</source>
        <comment>Wind direction</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="143"/>
        <source>Visibility</source>
        <translation>Видимость</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="147"/>
        <source>QNH</source>
        <translation>QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="211"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="212"/>
        <source>Value</source>
        <translation type="unfinished">Значение</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="218"/>
        <source>Wind from</source>
        <translation>Ветер с </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="226"/>
        <source>Distance in %1</source>
        <comment>%1 is unit</comment>
        <translation>Дальность в %1</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="vanished">Направление </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="240"/>
        <source>Speed in %1</source>
        <comment>%1 is unit</comment>
        <translation>Скорость в %1</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="vanished">Скорость</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="254"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="255"/>
        <source>Tasks</source>
        <translation>Задача</translation>
    </message>
    <message>
        <source>0m</source>
        <translation type="obsolete">0m (у земли)</translation>
    </message>
    <message>
        <source>2000m</source>
        <translation type="obsolete">2000м</translation>
    </message>
    <message>
        <source>8000m</source>
        <translation type="obsolete">8000м</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="284"/>
        <source>Waiting for mission datas...</source>
        <translation>Ожидание данных...</translation>
    </message>
</context>
<context>
    <name>ProfileDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>For all</source>
        <translation>Для всех</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Only for friends</source>
        <translation>Только для друзей</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Disable</source>
        <translation>Откл</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="25"/>
        <source>See all (no radar management)</source>
        <translation>Видеть всех (нет РЛС управления)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="26"/>
        <source>Cylinder (no altitude management)</source>
        <translation>Цилиндрическая (нет данных по высоте)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="27"/>
        <source>Spherical (altitude is used)</source>
        <translation>Сферическая (работает по высоте)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="28"/>
        <source>Conical (like spherical but with real constraints)</source>
        <translation>Коническая (как сферическая, но с реальными ограничениями)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="30"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="31"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Disabled</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Only coalition</source>
        <translation>Только каолиция</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="34"/>
        <source>Profile</source>
        <translation>Профиль</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="36"/>
        <source>Theater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="38"/>
        <source>Show enemies parameters (name, type,...)</source>
        <translation>Показать параметры противника (имя, тип, ...)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="40"/>
        <source>Show enemies SAM/Ships when out of radar</source>
        <translation>Показать ПВО/корабли противника вне радара</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="42"/>
        <source>Low speed airplanes are filtered</source>
        <translation>Не показывать низкоскоростные ЛА </translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="44"/>
        <source>Use relief</source>
        <translation>Использовать рельеф</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="46"/>
        <source>Automatic type for new item</source>
        <translation>Автоматический тип для новой еденицы</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="48"/>
        <source>Radar modelisation</source>
        <translation>Модель радара</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="50"/>
        <source>Approach</source>
        <translation>Подход</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="52"/>
        <source>Virtual awacs name (nothing=desactivated)</source>
        <translation>Виртуальный AWACS/ДРЛО (пусто = нет)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="54"/>
        <source>Virtual awacs range</source>
        <translation>Дальность виртуального AWACS/ДРЛО</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="56"/>
        <source>Minimal detection limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="58"/>
        <source>Force player name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="60"/>
        <source>Neutral coalition by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="67"/>
        <source>Server parameters</source>
        <translation>Параметры сервера</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="72"/>
        <source>Name</source>
        <comment>Name of server parameter</comment>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="73"/>
        <source>Value</source>
        <comment>Value for the server parameter</comment>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>RadioManager</name>
    <message>
        <source>Radio 1</source>
        <comment>Name of the radio 1</comment>
        <translation type="vanished">Радио 1</translation>
    </message>
    <message>
        <source>Radio 2</source>
        <comment>Name of the radio 2</comment>
        <translation type="vanished">Радио 2</translation>
    </message>
    <message>
        <source>Radio 3</source>
        <comment>Name of the radio 3</comment>
        <translation type="vanished">Радио 3</translation>
    </message>
    <message>
        <location filename="../lotatc/client/radio/RadioManager.cpp" line="10"/>
        <source>Radio %1</source>
        <comment>Name of the radio n</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerInfo</name>
    <message>
        <source>blue</source>
        <comment>coalition blue</comment>
        <translation type="vanished">Синяя</translation>
    </message>
    <message>
        <source>red</source>
        <comment>coalition red</comment>
        <translation type="vanished">Красная</translation>
    </message>
</context>
<context>
    <name>ServerModel</name>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="116"/>
        <source>New</source>
        <comment>New bookmark</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="151"/>
        <source>Default Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="153"/>
        <source>Default Red</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>There is something wrong with your license. Please contact support.</source>
        <translation>Что то не так с лицензией. Пожалуйста, обратитесь в техподдержку.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>Not registered&lt;br&gt;Put your %1 in the &lt;u&gt;%2 directory&lt;/u&gt;</source>
        <translation>Незарегистрированная копия &lt;br&gt;Поместите %1 в &lt;u&gt;%2 директорию &lt;/u&gt;</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="51"/>
        <source>If you do not have a license key &lt;u&gt;click HERE&lt;/u&gt; to buy one!</source>
        <translation>Если у вас нет лицензионного ключа &lt;u&gt;кликните сюда&lt;/u&gt; для покупки!</translation>
    </message>
</context>
<context>
    <name>TableObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="51"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="54"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="59"/>
        <source>Type</source>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="62"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="73"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="80"/>
        <source>Detection range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="87"/>
        <source>Threat range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="94"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolsSideBar</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="16"/>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="23"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="30"/>
        <source>Weather</source>
        <translation>Погода</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="38"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="46"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
</context>
<context>
    <name>TrollerItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerItem.qml" line="29"/>
        <source>Airports:</source>
        <translation>Аэродромы:</translation>
    </message>
</context>
<context>
    <name>TrollerList</name>
    <message>
        <source>Airport</source>
        <translation type="vanished">Аэродром</translation>
    </message>
    <message>
        <source>Radar</source>
        <translation type="vanished">Радар</translation>
    </message>
    <message>
        <source>My settings</source>
        <translation type="vanished">Мои настройки</translation>
    </message>
    <message>
        <source>Choose my position</source>
        <translation type="vanished">Моя позиция</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Тип</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="11"/>
        <source>Coalition trollers</source>
        <translation>Каолиция пользователя</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="24"/>
        <source>List of airports</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Unit</name>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="12"/>
        <source>Metric</source>
        <translation>Метрическая</translation>
    </message>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="13"/>
        <source>Imperial</source>
        <translation>Английская</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="12"/>
        <source>(Not registered)</source>
        <translation>Не зарегистрировано</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="268"/>
        <source>Disconnected</source>
        <translation>Разрыв соединения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="275"/>
        <source>Connection and authentification OK</source>
        <translation>Верификация пройдена</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="284"/>
        <source>Connection and authentification error: </source>
        <translation>Ошибка соединения и проверки:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="306"/>
        <source>New mission received, load data from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="329"/>
        <source>New version available: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="827"/>
        <source>Connecting to %1:%2...</source>
        <comment>%1 will be replaced by server, %2 by port</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="829"/>
        <source>Loading data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="831"/>
        <source>Not connected - not registered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="950"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="956"/>
        <source>Use bullseye as reference for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New mission received, load datas from server</source>
        <translation type="vanished">Передача новой миссии, загрузка данных с сервера</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="323"/>
        <source>Connection error: </source>
        <translation>Ошибка соединения:</translation>
    </message>
    <message>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation type="vanished">Аркадный</translation>
    </message>
    <message>
        <source>Basic</source>
        <comment>profile</comment>
        <translation type="vanished">Основной</translation>
    </message>
    <message>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation type="vanished">Реалистичный</translation>
    </message>
    <message>
        <source>Custom</source>
        <comment>profile</comment>
        <translation type="vanished">Персонализция</translation>
    </message>
    <message>
        <source>Mission</source>
        <comment>profile</comment>
        <translation type="vanished">Миссия</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="743"/>
        <source>Tactical View</source>
        <translation>Тактический вид</translation>
    </message>
    <message>
        <source>SITAC</source>
        <translation type="vanished">SITAC</translation>
    </message>
    <message>
        <source>Advanced</source>
        <comment>profile</comment>
        <translation type="obsolete">Продвинутый</translation>
    </message>
    <message>
        <source>Off</source>
        <translation type="obsolete">Выкл</translation>
    </message>
    <message>
        <source>On</source>
        <translation type="obsolete">Вкл</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="obsolete">Скрыть</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="obsolete">Показать</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="obsolete">Синий</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="obsolete">Красный</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="obsolete">Светлый</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="obsolete">Темный</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Английский</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="obsolete">Французский</translation>
    </message>
    <message>
        <source>Deutsch</source>
        <translation type="obsolete">Немецкий</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Итальянский</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Польский</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="obsolete">Чешский</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Португальский</translation>
    </message>
    <message>
        <source>Serbian</source>
        <translation type="obsolete">Сербский</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Испанский</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="obsolete">Китайский</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Русский</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Шведский</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="832"/>
        <source>Not connected</source>
        <translation>Нет соединения</translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation type="vanished">Соединение...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="932"/>
        <source>Change &amp;unit</source>
        <translation>Изменен &amp;юнит</translation>
    </message>
    <message>
        <source>Change unit</source>
        <translation type="vanished">Сменить юнит</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="938"/>
        <source>Show circles</source>
        <translation>Показать зоны</translation>
    </message>
    <message>
        <source>Show detection/threat circles</source>
        <translation type="vanished">Показать зоны обнаружения/угроз</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="944"/>
        <source>Show background label</source>
        <translation>Показать фоновые метки</translation>
    </message>
    <message>
        <source>Show/Hide background labels</source>
        <translation type="vanished">Показать/Скрыть фоновые метки</translation>
    </message>
    <message>
        <source>Disable fullscreen</source>
        <translation type="obsolete">Оконный режим</translation>
    </message>
    <message>
        <source>Enable fullscreen</source>
        <translation type="obsolete">Полноэкранный режим</translation>
    </message>
    <message>
        <source>Show fullscreen</source>
        <translation type="vanished">Развернуть до полноэкранного</translation>
    </message>
</context>
<context>
    <name>main_android</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="86"/>
        <source>Disconnected</source>
        <translation>Разрыв соединения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="93"/>
        <source>Connection and authentification OK</source>
        <translation>Верификация пройдена</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="99"/>
        <source>Connection and authentification error: </source>
        <translation>Ошибка соединения и проверки:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="121"/>
        <source>New mission received, load datas from server</source>
        <translation>Передача новой миссии, загрузка данных с сервера</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="137"/>
        <source>Connection error: </source>
        <translation>Ошибка соединения:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="213"/>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation>Аркадный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="214"/>
        <source>Basic</source>
        <comment>profile</comment>
        <translation>Основной</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="215"/>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation>Реалистичный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="216"/>
        <source>Custom</source>
        <comment>profile</comment>
        <translation>Настроенный</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="311"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="316"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="321"/>
        <source>Weather</source>
        <translation>Погода</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="326"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="331"/>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="336"/>
        <source>Glide</source>
        <translation>Глиссада</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="341"/>
        <source>Draw</source>
        <translation>Рисунок</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="348"/>
        <source>Airport</source>
        <translation>Аэродром</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="361"/>
        <source>SITAC</source>
        <translation>Общий вид</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="408"/>
        <source>Not connected</source>
        <translation>Нет соединения</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="578"/>
        <source>Change &amp;unit</source>
        <translation>Изменения &amp;системы</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="581"/>
        <source>Change unit</source>
        <translation>Изменить систему</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="585"/>
        <source>Show circles</source>
        <translation>Показать зоны</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="588"/>
        <source>Show detection/threat circles</source>
        <translation>Показать зоны обнаружения/угроз</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="592"/>
        <source>Show background label</source>
        <translation>Показать фоновые метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="595"/>
        <source>Show/Hide background labels</source>
        <translation>Показать/Скрыть фоновые метки</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Disable fullscreen</source>
        <translation>Выкл. полноэкранный режим</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Enable fullscreen</source>
        <translation>Вкл. полноэкранный режим</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="614"/>
        <source>Show fullscreen</source>
        <translation>Полноэкранный</translation>
    </message>
</context>
</TS>
