<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="51"/>
        <source>Author: DArt</source>
        <translation>Skapare: DArt</translation>
    </message>
    <message>
        <source>Thanks to all beta-testers and translators who help me</source>
        <translation type="vanished">Tack till alla betatestare och översättare som hjälpt mig</translation>
    </message>
    <message>
        <source>Thanks to my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation type="vanished">Tack till min skvadron &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Special mention for Azrayen&apos; for the testing and documentation awesome work.</source>
        <translation type="vanished">Jag vill speciellt nämna Azrayen för hans testning och dokumentation. Beundransvärt arbete.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="28"/>
        <source>THANKS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="37"/>
        <source>my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="40"/>
        <source>Azrayen&apos; and sp@t for the testing and documentation awesome work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="41"/>
        <source>Toubib for its work on Nevada map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="42"/>
        <source>Snoopy -76th vFS- for its charts and the work on airport views.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="43"/>
        <source>all beta-testers and translators who help me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="48"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="49"/>
        <source>Build: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="50"/>
        <source>User id: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="61"/>
        <source>Translators:</source>
        <translation>Översättare:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="72"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="73"/>
        <source>User</source>
        <translation>Translator</translation>
    </message>
</context>
<context>
    <name>AirportProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="107"/>
        <source>Open a chart view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="119"/>
        <source>METAR : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="129"/>
        <source>Create ATIS on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Release approach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Take approach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="223"/>
        <source>No control available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="236"/>
        <source>ILS available on %1MHz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="239"/>
        <source>No ILS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="242"/>
        <source>Current controllers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AirportPropertiesModel</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="27"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="28"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="29"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="30"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="31"/>
        <source>Altitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="32"/>
        <source>QFE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="33"/>
        <source>QNH</source>
        <translation type="unfinished">QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="34"/>
        <source>Declinaison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="35"/>
        <source>Heading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="36"/>
        <source>Frequencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="38"/>
        <source>TACAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BlockType</name>
    <message>
        <location filename="../lotatc/client/block/BlockType.cpp" line="19"/>
        <source>UNK</source>
        <comment>Short version of unknow</comment>
        <translation>OKÄ</translation>
    </message>
</context>
<context>
    <name>Braa</name>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="88"/>
        <source>H</source>
        <comment>Hot ex: 45/23Nm/1200ft/H</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="91"/>
        <source>FL</source>
        <comment>Flanking Left ex: 45/23Nm/1200ft/FL</comment>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="90"/>
        <source>C</source>
        <comment>Cold ex: 45/23Nm/1200ft/C</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="89"/>
        <source>FR</source>
        <comment>Flanking Right ex: 45/23Nm/1200ft/FR</comment>
        <translation>FR</translation>
    </message>
</context>
<context>
    <name>BullseyeProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="45"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="62"/>
        <source>Bullseye properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="74"/>
        <source>Set coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="83"/>
        <source>Restore from mission</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CarrierProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="38"/>
        <source>Ship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="39"/>
        <source>Airport</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatManager</name>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="18"/>
        <source>My side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="22"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="14"/>
        <source>LotAtc only</source>
        <translation>Endast LotAtc</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ChatMessage.qml" line="57"/>
        <source>Me:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/ColorButton.qml" line="95"/>
        <source>Click to select, Shift+click to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="129"/>
        <source>New bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="136"/>
        <source>Save current bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="160"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="171"/>
        <source>Enter a name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="180"/>
        <source>Address:</source>
        <translation>Adress (IP/DNS):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="188"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="198"/>
        <source>Coalition:</source>
        <translation>Sida:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="210"/>
        <source>Password:</source>
        <translation>Lösenord:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="220"/>
        <source>Pseudo:</source>
        <translation>Namn:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="167"/>
        <source>Name:</source>
        <translation type="unfinished">Namn:</translation>
    </message>
    <message>
        <source>Bookmark:</source>
        <translation type="vanished">Bokmärke:</translation>
    </message>
    <message>
        <source>Bookmark current</source>
        <translation type="vanished">Bokmärk nuvarande</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="146"/>
        <source>Delete current bookmark</source>
        <translation>Ta bort nuvarande bokmärke</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Connect</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Waiting maps...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="245"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>CoordinatesDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="56"/>
        <source>Enter coordinates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="63"/>
        <source>Format:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Restore</source>
        <comment>Restore the window</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Maximize</source>
        <comment>Maximize the window</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="67"/>
        <source>Minimize</source>
        <comment>Minimize the window</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="72"/>
        <source>Make tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="81"/>
        <source>Make dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="89"/>
        <source>Make dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="97"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="118"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LabelEdit</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="25"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="37"/>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="56"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="44"/>
        <source>Hovered</source>
        <translation>Svävande</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="60"/>
        <source>Connect</source>
        <translation type="unfinished">Anslut</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="63"/>
        <source>Connect to a server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="68"/>
        <source>Disconnect</source>
        <translation type="unfinished">Koppla från</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="71"/>
        <source>Disconnect current session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="79"/>
        <source>Open options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="86"/>
        <source>New version available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="94"/>
        <source>Display online help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="100"/>
        <source>Information on this software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="108"/>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="115"/>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="121"/>
        <source>Enter/Exit fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="127"/>
        <source>On top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="129"/>
        <source>Activate on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="138"/>
        <source>Lock map scale/move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="147"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="194"/>
        <source>Click to open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="232"/>
        <source>Change bearing mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="238"/>
        <source>Change units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="276"/>
        <source>Mission time, show/hide seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="288"/>
        <source>Show controllers informations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="296"/>
        <source>Show server settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Alternativ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="83"/>
        <source>News</source>
        <translation type="unfinished">Nyheter</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjälp</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Om</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>METRIC</source>
        <translation type="unfinished">METRISK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>IMPERIAL</source>
        <translation type="unfinished">IMPERIALISTISK</translation>
    </message>
</context>
<context>
    <name>MapItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItem.qml" line="224"/>
        <source>%1</source>
        <translation type="unfinished">%1</translation>
    </message>
</context>
<context>
    <name>MapItemProperties</name>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Okänd</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="75"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="76"/>
        <source>TN: %1</source>
        <comment>Track Number of the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="91"/>
        <source>Group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="93"/>
        <source>Transponder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="94"/>
        <source>Altitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="95"/>
        <source>Heading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="96"/>
        <source>Ground Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="98"/>
        <source>Vertical Indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="99"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="100"/>
        <source>BullsEye</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="104"/>
        <source>Detection range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="106"/>
        <source>Threat range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="139"/>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation type="unfinished">Namn:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="152"/>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="245"/>
        <source>Apply</source>
        <translation type="unfinished">Verkställ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="171"/>
        <source>Side:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="198"/>
        <source>Symbol:</source>
        <translation type="unfinished">Ikon:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="231"/>
        <source>Comment:</source>
        <comment>Comment on the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="272"/>
        <source>Message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="285"/>
        <source>Text to send...</source>
        <translation type="unfinished">Meddelande du vill skicka...</translation>
    </message>
</context>
<context>
    <name>MyDockDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/MyDockDialog.qml" line="62"/>
        <source>Waiting connection...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyModels</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="48"/>
        <source>Off</source>
        <translation type="unfinished">Av</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="49"/>
        <source>On</source>
        <translation type="unfinished">På</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="55"/>
        <source>Hide</source>
        <translation type="unfinished">Göm</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="56"/>
        <source>Show</source>
        <translation type="unfinished">Visa</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="68"/>
        <source>Neutral</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="80"/>
        <source>Blue</source>
        <translation type="unfinished">Blå</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="81"/>
        <source>Red</source>
        <translation type="unfinished">Röd</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="123"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="124"/>
        <source>Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="125"/>
        <source>Enemy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="140"/>
        <source>Light</source>
        <translation type="unfinished">Ljus</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="141"/>
        <source>Dark</source>
        <translation type="unfinished">Mörk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="142"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="183"/>
        <source>English</source>
        <translation type="unfinished">Engelsk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="184"/>
        <source>French</source>
        <translation type="unfinished">Fransk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="185"/>
        <source>Deutsch</source>
        <translation type="unfinished">Tysk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="186"/>
        <source>Italian</source>
        <translation type="unfinished">Italiensk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="187"/>
        <source>Polish</source>
        <translation type="unfinished">Polsk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="188"/>
        <source>Czech</source>
        <translation type="unfinished">Tjeckisk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="189"/>
        <source>Portuguese</source>
        <translation type="unfinished">Portuguisisk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="190"/>
        <source>Serbian</source>
        <translation type="unfinished">Serbisk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="191"/>
        <source>Spanish</source>
        <translation type="unfinished">Spansk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="192"/>
        <source>Chinese</source>
        <translation type="unfinished">Kinesisk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="193"/>
        <source>Russian</source>
        <translation type="unfinished">Rysk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="194"/>
        <source>Swedish</source>
        <translation type="unfinished">Svenska</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="195"/>
        <source>Brazilian Portuguese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="210"/>
        <source>Solid</source>
        <comment>Solid pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="211"/>
        <source>Dense</source>
        <comment>Dense pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="212"/>
        <source>Horizontal</source>
        <comment>Horizontal pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="213"/>
        <source>Vertical</source>
        <comment>Horizontal pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="214"/>
        <source>Diagonal</source>
        <comment>Diagonal pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="215"/>
        <source>Cross</source>
        <comment>Cross pattern brush</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="223"/>
        <source>Unit Name (or TN)</source>
        <translation type="unfinished">Enhetsnamn (eller TN)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="224"/>
        <source>%(name|10) limit name to 10 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="228"/>
        <source>Group Name (if available)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="229"/>
        <source>%(group_name|10) limit name to 10 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="233"/>
        <source>Unit type</source>
        <translation type="unfinished">Enhetstyp</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="238"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="239"/>
        <source>%(comment|10) limit comment to 10 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="243"/>
        <source>Unit heading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="244"/>
        <source>Use %(heading|option) with option=true/mag/truemag for true/magnetic deviation correction (default is auto)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="248"/>
        <source>Altitude long format</source>
        <translation type="unfinished">Höjd, långt format</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="253"/>
        <source>Altitude short format</source>
        <translation type="unfinished">Höjd, kort format</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="258"/>
        <source>Ground speed</source>
        <translation type="unfinished">Färdhastighet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="263"/>
        <source>Ground speed in short format</source>
        <translation type="unfinished">Färdhastighet, kort format</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="268"/>
        <source>Ground speed unit</source>
        <translation type="unfinished">Färdhastighet, enhet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="273"/>
        <source>Ground speed in Mach format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="278"/>
        <source>Position to BullsEye</source>
        <translation type="unfinished">Position till Bullseye</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="283"/>
        <source>Vertical indicator</source>
        <translation type="unfinished">Vertikalindikator</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="284"/>
        <source>%(vert_indic|nostable) to avoid stable cursor, %(vert_indic|n) with n=0/1/2 for different arrows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="288"/>
        <source>Transponder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="299"/>
        <source>Decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="300"/>
        <source>Longitude/Latitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="301"/>
        <source>Longitude/Latitude Decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="302"/>
        <source>MGRS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="303"/>
        <source>UTM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="309"/>
        <source>HF</source>
        <comment>HF frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="310"/>
        <source>UHF</source>
        <comment>UHF frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="311"/>
        <source>VHF</source>
        <comment>VHF frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="317"/>
        <source>AM</source>
        <comment>AM frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="318"/>
        <source>FM</source>
        <comment>FM frequency band for radio</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="333"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="334"/>
        <source>OpenGL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="335"/>
        <source>DirectX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="351"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="352"/>
        <source>Mid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="353"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="369"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="370"/>
        <source>Low (4x)</source>
        <comment>antialiasing to 4x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="371"/>
        <source>Mid (8x)</source>
        <comment>antialiasing to 8x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="372"/>
        <source>High (16x)</source>
        <comment>antialiasing to 16x</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="390"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="391"/>
        <source>Detection profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="428"/>
        <source>Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="430"/>
        <source>Show circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="432"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="433"/>
        <source>Use bullseye as reference for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>True</source>
        <comment>True bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>T</source>
        <comment>Shortcut for True bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>Magnetic</source>
        <comment>Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>M</source>
        <comment>Shortcut for Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>True/Magnetic</source>
        <comment>True/Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>T+M</source>
        <comment>Shortcut for True+Magnetic bearing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="475"/>
        <source>Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="476"/>
        <source>Helicopter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="477"/>
        <source>Ground moving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="478"/>
        <source>Ground standing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="479"/>
        <source>Ship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="480"/>
        <source>Sam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="481"/>
        <source>Tank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="482"/>
        <source>Airport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="497"/>
        <source>Normal - Small font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="498"/>
        <source>Normal</source>
        <translation type="unfinished">Normal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="499"/>
        <source>Normal - Large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="500"/>
        <source>Normal - Very large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="501"/>
        <source>Dense - Small font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="502"/>
        <source>Dense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="503"/>
        <source>Dense - Large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="504"/>
        <source>Dense - Very large font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="389"/>
        <source>Labels</source>
        <translation type="unfinished">Beteckning</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="254"/>
        <source>%(alti_short|thousand) in imperial, show altitude in thousand feets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="429"/>
        <source>Show background label</source>
        <translation type="unfinished">Visa bakgrund på beteckning</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="431"/>
        <source>Change unit</source>
        <translation type="unfinished">Ändra enhet</translation>
    </message>
</context>
<context>
    <name>MyWebView</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
</context>
<context>
    <name>NetDrawingText</name>
    <message>
        <location filename="../lotatc/common/NetDrawingText.cpp" line="55"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetObject</name>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="161"/>
        <source>You try to connect to an older incompatible version, server should be upgraded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="163"/>
        <source>You try to connect to an newer incompatible version, client should be upgraded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewsDlg</name>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Laddar...</translation>
    </message>
    <message>
        <source>News</source>
        <translation type="vanished">Nyheter</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fel</translation>
    </message>
</context>
<context>
    <name>OptionsApproach</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="15"/>
        <source>Number of contact updates to display on glide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="32"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="20"/>
        <source>Play sound on new messages</source>
        <translation type="unfinished">Ljud vid nytt meddelande</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="26"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="30"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="35"/>
        <source>Please choose a color for background chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="39"/>
        <source>Foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="44"/>
        <source>Please choose a color for foreground chat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="15"/>
        <source>General</source>
        <translation>Allmänt</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="16"/>
        <source>Items</source>
        <translation>Ikoner</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="17"/>
        <source>Map</source>
        <translation type="unfinished">Karta</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="18"/>
        <source>Labels</source>
        <translation>Beteckning</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="19"/>
        <source>Approach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="20"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="obsolete">Välj språk (kräver omstart):</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="obsolete">Välj ett tema för programmet (kräver omstart):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="21"/>
        <source>Chat</source>
        <translation type="unfinished">Chatt</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="obsolete">Ljud vid nytt meddelande</translation>
    </message>
    <message>
        <source>Choose symbology</source>
        <translation type="obsolete">Välj symbolik</translation>
    </message>
    <message>
        <source>Define color for friends</source>
        <translation type="obsolete">Färg på egna sidan</translation>
    </message>
    <message>
        <source>Choose color</source>
        <translation type="obsolete">Välj färg</translation>
    </message>
    <message>
        <source>Please choose a color for friend unit</source>
        <translation type="obsolete">Var god välj en färg för egna enheter</translation>
    </message>
    <message>
        <source>Default color</source>
        <translation type="obsolete">Standardfärg</translation>
    </message>
    <message>
        <source>Define color for enemies</source>
        <translation type="obsolete">Välj färg för fientliga enheter</translation>
    </message>
    <message>
        <source>Please choose a color for enemies unit</source>
        <translation type="obsolete">Var god välj en färg för fientliga enheter</translation>
    </message>
    <message>
        <source>Define size of Item</source>
        <translation type="obsolete">Definiera storlek på ikoner</translation>
    </message>
    <message>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation type="obsolete">Definiera fartvektorns läge (välj 0s för förstoringsoberoende)</translation>
    </message>
    <message>
        <source>%1 seconds</source>
        <translation type="obsolete">%1 sekunder</translation>
    </message>
    <message>
        <source>Zoom independent</source>
        <translation type="obsolete">Förstoringsoberoende</translation>
    </message>
    <message>
        <source>Define labels for friends</source>
        <translation type="obsolete">Definiera beteckning för egna enheter</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="obsolete">Definiera beteckning för fientliga enheter</translation>
    </message>
    <message>
        <source>Show background for label (Key: Alt+L)</source>
        <translation type="obsolete">Visa bakgrund för beteckning (Alt+L)</translation>
    </message>
    <message>
        <source>Size of font label:</source>
        <translation type="obsolete">Teckenstorlek etikett:</translation>
    </message>
    <message>
        <source>Available values:</source>
        <translation type="obsolete">Tillgängliga värden:</translation>
    </message>
    <message>
        <source>Unit Name (or TN)</source>
        <translation type="obsolete">Enhetsnamn (eller TN)</translation>
    </message>
    <message>
        <source>Unit type</source>
        <translation type="obsolete">Enhetstyp</translation>
    </message>
    <message>
        <source>Altitude long format</source>
        <translation type="obsolete">Höjd, långt format</translation>
    </message>
    <message>
        <source>Altitude short format</source>
        <translation type="obsolete">Höjd, kort format</translation>
    </message>
    <message>
        <source>Ground speed</source>
        <translation type="obsolete">Färdhastighet</translation>
    </message>
    <message>
        <source>Ground speed in short format</source>
        <translation type="obsolete">Färdhastighet, kort format</translation>
    </message>
    <message>
        <source>Ground speed unit</source>
        <translation type="obsolete">Färdhastighet, enhet</translation>
    </message>
    <message>
        <source>Position to BullsEye</source>
        <translation type="obsolete">Position till Bullseye</translation>
    </message>
    <message>
        <source>Vertical indicator</source>
        <translation type="obsolete">Vertikalindikator</translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="obsolete">Klicka här för fullständig dokumentation</translation>
    </message>
</context>
<context>
    <name>OptionsGeneral</name>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="obsolete">Välj språk (kräver omstart):</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="obsolete">Välj ett tema för programmet (kräver omstart):</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="obsolete">Ljud vid nytt meddelande</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="27"/>
        <source>Choose a language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="44"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="68"/>
        <source>Choose a variant for the UI (size of the UI):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="83"/>
        <source>Choose a theme for the application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="98"/>
        <source>Choose a variation color for the application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="113"/>
        <source>Restore default dock and windows positions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="115"/>
        <source>Click to reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="127"/>
        <source>Check update at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="134"/>
        <source>Performances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="140"/>
        <source>Graphical effects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="160"/>
        <source>Antialiasing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="172"/>
        <source>(*) : Need application restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="66"/>
        <source>Choose symbology</source>
        <translation type="unfinished">Välj symbolik</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="45"/>
        <source>Please choose a color for enemies unit</source>
        <translation type="unfinished">Var god välj en färg för fientliga enheter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="27"/>
        <source>Define color for items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="32"/>
        <source>Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="36"/>
        <source>Please choose a color for friends unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="41"/>
        <source>Enemies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="49"/>
        <source>Neutral</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="53"/>
        <source>Please choose a color for neutral unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="62"/>
        <source>Items parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="77"/>
        <source>Define size of Item</source>
        <translation type="unfinished">Definiera storlek på ikoner</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="90"/>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation type="unfinished">Definiera fartvektorns läge (välj 0s för förstoringsoberoende)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>Zoom independent</source>
        <translation type="unfinished">Förstoringsoberoende</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>%1 seconds</source>
        <translation type="unfinished">%1 sekunder</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="111"/>
        <source>Define number of ghost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="127"/>
        <source>Define opacity of circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="132"/>
        <source>Detection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="147"/>
        <source>Threat:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsLabel</name>
    <message>
        <source>Define labels for friends</source>
        <translation type="obsolete">Definiera beteckning för egna enheter</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="obsolete">Definiera beteckning för fientliga enheter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="19"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="23"/>
        <source>Show background for label (Key: Alt+L)</source>
        <translation type="unfinished">Visa bakgrund för beteckning (Alt+L)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="29"/>
        <source>Size of font label:</source>
        <translation type="unfinished">Teckenstorlek etikett:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="40"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="44"/>
        <source>Please choose a color for label background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="48"/>
        <source>Foreground:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="52"/>
        <source>Please choose a color for label foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="56"/>
        <source>Background mode alert 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="60"/>
        <source>Please choose a color for label background in simple alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="64"/>
        <source>Background mode alert 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="68"/>
        <source>Please choose a color for label background in alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="76"/>
        <source>Define contents for labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="79"/>
        <source>Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="80"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="126"/>
        <source>Available values:</source>
        <translation type="unfinished">Tillgängliga värden:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="138"/>
        <source>Full documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="obsolete">Klicka här för fullständig dokumentation</translation>
    </message>
</context>
<context>
    <name>OptionsMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="20"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="24"/>
        <source>Land</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="29"/>
        <source>Please choose a color for land</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="33"/>
        <source>Sea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="38"/>
        <source>Please choose a color for sea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="42"/>
        <source>Range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="54"/>
        <source>Please choose a color for range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="58"/>
        <source>Measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="63"/>
        <source>Please choose a color for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="67"/>
        <source>Measure line background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="72"/>
        <source>Please choose a color for background measure line label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="76"/>
        <source>BRAA line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="81"/>
        <source>Please choose a color for braa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="88"/>
        <source>Advanced</source>
        <translation type="unfinished">Avancerad</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="94"/>
        <source>Brightness</source>
        <translation type="unfinished">Ljusstyrka</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="112"/>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="135"/>
        <source>Default</source>
        <translation type="unfinished">Standard</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="116"/>
        <source>Contrast</source>
        <translation type="unfinished">Kontrast</translation>
    </message>
</context>
<context>
    <name>OptionsShortcut</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="30"/>
        <source>You can change shortcuts here, just double-clic on it to change it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="61"/>
        <source>Click on a shortcut to edit it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="70"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="75"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageAirportView</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageAirportView.qml" line="12"/>
        <source>Airport view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageBraa</name>
    <message>
        <source>Show BRAA on map:</source>
        <translation type="vanished">Visa BRAA på karta:</translation>
    </message>
    <message>
        <source>Show BRAA text:</source>
        <translation type="vanished">Visa BRAA text:</translation>
    </message>
    <message>
        <source>List of all BRAA in use</source>
        <translation type="vanished">Alla aktiva BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="52"/>
        <source>Show</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="170"/>
        <source>Unit</source>
        <translation>Enhet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="145"/>
        <source>Target</source>
        <translation>Mål</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="13"/>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="146"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="24"/>
        <source>list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="25"/>
        <source>Options</source>
        <translation type="unfinished">Alternativ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="90"/>
        <source>SendIt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="144"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="149"/>
        <source>Interception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>METRIC</source>
        <translation type="unfinished">METRISK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>IMPERIAL</source>
        <translation type="unfinished">IMPERIALISTISK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="186"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="216"/>
        <source>Delete selected</source>
        <translation>Ta bort vald</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="234"/>
        <source>Delete all</source>
        <translation>Ta bort alla</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="253"/>
        <source>Show BRAA on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="259"/>
        <source>Show BRAA text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="266"/>
        <source>Show BRAA interception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="274"/>
        <source>Send to pilot with unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="279"/>
        <source>Send to pilot every:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Ta bort</translation>
    </message>
</context>
<context>
    <name>PageChart</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChart.qml" line="14"/>
        <source>Chart view for %1</source>
        <comment>%1 replaced by airport name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="12"/>
        <source>Chat</source>
        <translation type="unfinished">Chatt</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="64"/>
        <source>Text to send...</source>
        <translation>Meddelande du vill skicka...</translation>
    </message>
</context>
<context>
    <name>PageDraw</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="14"/>
        <source>Draw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="95"/>
        <source>Please choose a font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="110"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="126"/>
        <source>Please choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="169"/>
        <source>Edit Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="177"/>
        <source>Add text on coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="185"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="195"/>
        <source>New draw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="210"/>
        <source>Shared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="212"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="235"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="217"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="239"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="222"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="243"/>
        <source>Polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="227"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="247"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="233"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="365"/>
        <source>Private</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="254"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="256"/>
        <source>All private draws</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="260"/>
        <source>All shared draws (WARNING)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="268"/>
        <source>Delete</source>
        <translation type="unfinished">Ta bort</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="279"/>
        <source>Save to file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="283"/>
        <source>Load from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="292"/>
        <source>Select a draw:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="336"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="344"/>
        <source>Name:</source>
        <translation type="unfinished">Namn:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="349"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="357"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="373"/>
        <source>Make it public:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="386"/>
        <source>Share it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="394"/>
        <source>Text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="400"/>
        <source>Enter text to display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="410"/>
        <source>Font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="416"/>
        <source>Choose font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="426"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="431"/>
        <source>Foreground color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="442"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="448"/>
        <source>Background color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="458"/>
        <source>Line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="479"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageGraph</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="18"/>
        <source>Approach %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="25"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="27"/>
        <source>Save graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="228"/>
        <source>Please choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="294"/>
        <source>Glidepath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="496"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="555"/>
        <source>Items</source>
        <translation type="unfinished">Ikoner</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="556"/>
        <source>Airport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="557"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="591"/>
        <source>Show</source>
        <translation type="unfinished">Visa</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="640"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="649"/>
        <source>Distance(%1)</source>
        <comment>%1 will be replaced by unit: km</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="669"/>
        <source>Release approach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="689"/>
        <source>Show labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="697"/>
        <source>Show circles on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="708"/>
        <source>Steady level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="726"/>
        <source>Minima:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="744"/>
        <source>Glide angle:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="774"/>
        <source>LSLLC:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Ta bort</translation>
    </message>
</context>
<context>
    <name>PageMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="51"/>
        <source>Options</source>
        <translation>Alternativ</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="50"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="obsolete">Avancerad</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="114"/>
        <source>Altitude filter</source>
        <translation>Höjdfilter</translation>
    </message>
    <message>
        <source>from</source>
        <translation type="obsolete">från</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">till</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="153"/>
        <source>Speed filter</source>
        <translation>Hastighetsfilter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="14"/>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="49"/>
        <source>Map</source>
        <translation>Karta</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="63"/>
        <source>Choose a map:</source>
        <translation type="unfinished">Välj en karta:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="77"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="100"/>
        <source>Please choose a color for layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="191"/>
        <source>Hide ground units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="215"/>
        <source>Show draw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="221"/>
        <source>Show measure line value on bottom of the window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="237"/>
        <source>Coordinates format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="251"/>
        <source>Example: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="257"/>
        <source>Show airport code instead of name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="vanished">Ljusstyrka</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="vanished">Kontrast</translation>
    </message>
</context>
<context>
    <name>PageObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="15"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="34"/>
        <source>Enter text to filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="52"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="69"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="91"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageProperties</name>
    <message>
        <source>Id: %1</source>
        <comment>Id of the unit</comment>
        <translation type="obsolete">Id %1</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Okänd</translation>
    </message>
    <message>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation type="vanished">Namn:</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Verkställ</translation>
    </message>
    <message>
        <source>Symbol:</source>
        <translation type="vanished">Ikon:</translation>
    </message>
    <message>
        <source>Text to send...</source>
        <translation type="obsolete">Meddelande du vill skicka...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Properties %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Selection&apos;s properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="24"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="26"/>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="120"/>
        <source>Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="34"/>
        <source>Make this object as new bullseye reference?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="35"/>
        <source>If yes, bullseye will be set to this object (only for you)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="98"/>
        <source>Select an item/airport to see its properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="125"/>
        <source>Show on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Set current virtual position at this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Need radio enabled and an active radar unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="148"/>
        <source>Display range rings at this object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="156"/>
        <source>Set this object as bullseye reference</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageRadio</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="12"/>
        <source>Radio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="27"/>
        <source>Airport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="40"/>
        <source>Radar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="75"/>
        <source>Radio enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="109"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>Current Location:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>NOT DEFINED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="167"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="168"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="174"/>
        <source>Refresh list (not automatic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="179"/>
        <source>Set my position to selected one</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageWeather</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="11"/>
        <source>Weather</source>
        <translation type="unfinished">Väder</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="142"/>
        <source>METAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="144"/>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="145"/>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="148"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="176"/>
        <source>Sky clear</source>
        <translation type="unfinished">Klart väder</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="177"/>
        <source>Some clouds</source>
        <translation type="unfinished">Brutna moln</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="178"/>
        <source>Clouds</source>
        <translation type="unfinished">Molnigt</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="179"/>
        <source>Overcast</source>
        <translation type="unfinished">Mulet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="180"/>
        <source>Rain</source>
        <translation type="unfinished">Regn</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="181"/>
        <source>Snow</source>
        <translation type="unfinished">Snö</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="182"/>
        <source>Thunderstorms</source>
        <translation type="unfinished">Åska</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="183"/>
        <source>Snow thunderstorms</source>
        <translation type="unfinished">Snö och åska</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="226"/>
        <source>Distance in %1</source>
        <comment>%1 is unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="233"/>
        <source>From</source>
        <comment>Wind direction</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="240"/>
        <source>Speed in %1</source>
        <comment>%1 is unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="254"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="255"/>
        <source>Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="143"/>
        <source>Visibility</source>
        <translation type="unfinished">Sikt</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="147"/>
        <source>QNH</source>
        <translation type="unfinished">QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="211"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="212"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="218"/>
        <source>Wind from</source>
        <translation type="unfinished">Vind från</translation>
    </message>
    <message>
        <source>0m</source>
        <translation type="obsolete">0m</translation>
    </message>
    <message>
        <source>2000m</source>
        <translation type="obsolete">2000m</translation>
    </message>
    <message>
        <source>8000m</source>
        <translation type="obsolete">8000m</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="284"/>
        <source>Waiting for mission datas...</source>
        <translation type="unfinished">Väntar på uppdragsdata...</translation>
    </message>
</context>
<context>
    <name>ProfileDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>For all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Only for friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="25"/>
        <source>See all (no radar management)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="26"/>
        <source>Cylinder (no altitude management)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="27"/>
        <source>Spherical (altitude is used)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="28"/>
        <source>Conical (like spherical but with real constraints)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="30"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="31"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Only coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="34"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="36"/>
        <source>Theater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="38"/>
        <source>Show enemies parameters (name, type,...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="40"/>
        <source>Show enemies SAM/Ships when out of radar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="42"/>
        <source>Low speed airplanes are filtered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="44"/>
        <source>Use relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="46"/>
        <source>Automatic type for new item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="48"/>
        <source>Radar modelisation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="50"/>
        <source>Approach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="52"/>
        <source>Virtual awacs name (nothing=desactivated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="54"/>
        <source>Virtual awacs range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="56"/>
        <source>Minimal detection limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="58"/>
        <source>Force player name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="60"/>
        <source>Neutral coalition by default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="67"/>
        <source>Server parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="72"/>
        <source>Name</source>
        <comment>Name of server parameter</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="73"/>
        <source>Value</source>
        <comment>Value for the server parameter</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadioManager</name>
    <message>
        <location filename="../lotatc/client/radio/RadioManager.cpp" line="10"/>
        <source>Radio %1</source>
        <comment>Name of the radio n</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerInfo</name>
    <message>
        <source>blue</source>
        <comment>coalition blue</comment>
        <translation type="vanished">Blå</translation>
    </message>
    <message>
        <source>red</source>
        <comment>coalition red</comment>
        <translation type="vanished">Röd</translation>
    </message>
</context>
<context>
    <name>ServerModel</name>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="116"/>
        <source>New</source>
        <comment>New bookmark</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="151"/>
        <source>Default Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="153"/>
        <source>Default Red</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>There is something wrong with your license. Please contact support.</source>
        <translation>Det är ett fel med din licens. Var god kontakta supporten.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>Not registered&lt;br&gt;Put your %1 in the &lt;u&gt;%2 directory&lt;/u&gt;</source>
        <translation>Ej registrerad &lt;br&gt;Lägg din %1 i &lt;u&gt;%2 mappen&lt;/u&gt;</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="51"/>
        <source>If you do not have a license key &lt;u&gt;click HERE&lt;/u&gt; to buy one!</source>
        <translation>Om du inte har en licens, &lt;u&gt;tryck här&lt;/u&gt; för att köpa en!</translation>
    </message>
</context>
<context>
    <name>TableObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="51"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="54"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="59"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="62"/>
        <source>Coalition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="73"/>
        <source>Human</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="80"/>
        <source>Detection range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="87"/>
        <source>Threat range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="94"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolsSideBar</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="16"/>
        <source>Map</source>
        <translation>Karta</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="23"/>
        <source>Properties</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="30"/>
        <source>Weather</source>
        <translation>Väder</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="38"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="46"/>
        <source>Chat</source>
        <translation>Chatt</translation>
    </message>
</context>
<context>
    <name>TrollerItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerItem.qml" line="29"/>
        <source>Airports:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrollerList</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="11"/>
        <source>Coalition trollers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="24"/>
        <source>List of airports</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Unit</name>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="12"/>
        <source>Metric</source>
        <translation>Metrisk</translation>
    </message>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="13"/>
        <source>Imperial</source>
        <translation>Imperialistisk</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="12"/>
        <source>(Not registered)</source>
        <translation type="unfinished">(Ej registrerad)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="268"/>
        <source>Disconnected</source>
        <translation type="unfinished">Frånkopplad</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="275"/>
        <source>Connection and authentification OK</source>
        <translation type="unfinished">Uppkoppling och autentisering OK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="284"/>
        <source>Connection and authentification error: </source>
        <translation type="unfinished">Fel på uppkoppling och autentiseringsfel:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="306"/>
        <source>New mission received, load data from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="329"/>
        <source>New version available: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="743"/>
        <source>Tactical View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="827"/>
        <source>Connecting to %1:%2...</source>
        <comment>%1 will be replaced by server, %2 by port</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="829"/>
        <source>Loading data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="831"/>
        <source>Not connected - not registered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="950"/>
        <source>Show range rings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="956"/>
        <source>Use bullseye as reference for measure line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New mission received, load datas from server</source>
        <translation type="obsolete">Nytt uppdrag mottaget, laddar data från server</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="323"/>
        <source>Connection error: </source>
        <translation type="unfinished">Anslutningsfel:</translation>
    </message>
    <message>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation type="obsolete">Arkad</translation>
    </message>
    <message>
        <source>Basic</source>
        <comment>profile</comment>
        <translation type="obsolete">Normal</translation>
    </message>
    <message>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation type="obsolete">Realistisk</translation>
    </message>
    <message>
        <source>Advanced</source>
        <comment>profile</comment>
        <translation type="obsolete">Avancerad</translation>
    </message>
    <message>
        <source>Off</source>
        <translation type="obsolete">Av</translation>
    </message>
    <message>
        <source>On</source>
        <translation type="obsolete">På</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="obsolete">Göm</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="obsolete">Visa</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="obsolete">Blå</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="obsolete">Röd</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="obsolete">Ljus</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="obsolete">Mörk</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Engelsk</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="obsolete">Fransk</translation>
    </message>
    <message>
        <source>Deutsch</source>
        <translation type="obsolete">Tysk</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Italiensk</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Polsk</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="obsolete">Tjeckisk</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Portuguisisk</translation>
    </message>
    <message>
        <source>Serbian</source>
        <translation type="obsolete">Serbisk</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Spansk</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="obsolete">Kinesisk</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Rysk</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Svenska</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="832"/>
        <source>Not connected</source>
        <translation type="unfinished">Ej ansluten</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="932"/>
        <source>Change &amp;unit</source>
        <translation type="unfinished">Ändra &amp;enhet</translation>
    </message>
    <message>
        <source>Change unit</source>
        <translation type="obsolete">Ändra enhet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="938"/>
        <source>Show circles</source>
        <translation type="unfinished">Visa cirklar</translation>
    </message>
    <message>
        <source>Show detection/threat circles</source>
        <translation type="obsolete">Visa upptäckt/hot- cirklar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="944"/>
        <source>Show background label</source>
        <translation type="unfinished">Visa bakgrund på beteckning</translation>
    </message>
    <message>
        <source>Show/Hide background labels</source>
        <translation type="obsolete">Visa/dölj bakgrund på beteckningar</translation>
    </message>
    <message>
        <source>Disable fullscreen</source>
        <translation type="obsolete">Stäng fullskärm</translation>
    </message>
    <message>
        <source>Enable fullscreen</source>
        <translation type="obsolete">Aktivera fullskärm</translation>
    </message>
    <message>
        <source>Show fullscreen</source>
        <translation type="obsolete">Visa fullskärm</translation>
    </message>
</context>
<context>
    <name>main_android</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="86"/>
        <source>Disconnected</source>
        <translation type="unfinished">Frånkopplad</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="93"/>
        <source>Connection and authentification OK</source>
        <translation type="unfinished">Uppkoppling och autentisering OK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="99"/>
        <source>Connection and authentification error: </source>
        <translation type="unfinished">Fel på uppkoppling och autentiseringsfel:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="121"/>
        <source>New mission received, load datas from server</source>
        <translation type="unfinished">Nytt uppdrag mottaget, laddar data från server</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="137"/>
        <source>Connection error: </source>
        <translation type="unfinished">Anslutningsfel:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="213"/>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation type="unfinished">Arkad</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="214"/>
        <source>Basic</source>
        <comment>profile</comment>
        <translation type="unfinished">Normal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="215"/>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation type="unfinished">Realistisk</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="216"/>
        <source>Custom</source>
        <comment>profile</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="311"/>
        <source>Chat</source>
        <translation type="unfinished">Chatt</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="316"/>
        <source>Properties</source>
        <translation type="unfinished">Info</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="321"/>
        <source>Weather</source>
        <translation type="unfinished">Väder</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="326"/>
        <source>BRAA</source>
        <translation type="unfinished">BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="331"/>
        <source>Map</source>
        <translation type="unfinished">Karta</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="336"/>
        <source>Glide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="341"/>
        <source>Draw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="348"/>
        <source>Airport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="361"/>
        <source>SITAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="408"/>
        <source>Not connected</source>
        <translation type="unfinished">Ej ansluten</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="578"/>
        <source>Change &amp;unit</source>
        <translation type="unfinished">Ändra &amp;enhet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="581"/>
        <source>Change unit</source>
        <translation type="unfinished">Ändra enhet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="585"/>
        <source>Show circles</source>
        <translation type="unfinished">Visa cirklar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="588"/>
        <source>Show detection/threat circles</source>
        <translation type="unfinished">Visa upptäckt/hot- cirklar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="592"/>
        <source>Show background label</source>
        <translation type="unfinished">Visa bakgrund på beteckning</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="595"/>
        <source>Show/Hide background labels</source>
        <translation type="unfinished">Visa/dölj bakgrund på beteckningar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Disable fullscreen</source>
        <translation type="unfinished">Stäng fullskärm</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Enable fullscreen</source>
        <translation type="unfinished">Aktivera fullskärm</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="614"/>
        <source>Show fullscreen</source>
        <translation type="unfinished">Visa fullskärm</translation>
    </message>
</context>
</TS>
