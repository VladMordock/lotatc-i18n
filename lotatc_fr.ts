<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ATCView</name>
    <message>
        <source>(Not registered)</source>
        <translation type="obsolete">(Non enregistré)</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="50"/>
        <source>User id: %1</source>
        <translation>Id d&apos;utilisateur: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="49"/>
        <source>Build: %1</source>
        <translation>Build: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="28"/>
        <source>THANKS:</source>
        <translation>MERCI:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="37"/>
        <source>my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation>mon escadrons: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="40"/>
        <source>Azrayen&apos; and sp@t for the testing and documentation awesome work.</source>
        <translation>Azrayen&apos;et sp@t pour leur impressionant travail sur le test et la documentation.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="41"/>
        <source>Toubib for its work on Nevada map.</source>
        <translation>Toubib pour son travail sur la carte Nevada.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="42"/>
        <source>Snoopy -76th vFS- for its charts and the work on airport views.</source>
        <translation>Snoopy -76th vFS- pour les charts et le travail sur les aéroports.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="43"/>
        <source>all beta-testers and translators who help me</source>
        <translation>tous les beta-testers et traducteurs qui m&apos;ont aidés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="48"/>
        <source>Version: %1</source>
        <translation>Version: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="51"/>
        <source>Author: DArt</source>
        <translation>Auteur: DArt</translation>
    </message>
    <message>
        <source>Thanks to all beta-testers and translators who help me</source>
        <translation type="vanished">Merci à tous les beta-testeurs et traducteurs qui m&apos;aident</translation>
    </message>
    <message>
        <source>Thanks to my squad: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</source>
        <translation type="vanished">Merci à mon escadrille: &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Special mention for Azrayen&apos; for the testing and documentation awesome work.</source>
        <translation type="vanished">Mention spéciale pour Azrayen&apos; pour son travail sur les tests et la documentation.</translation>
    </message>
    <message>
        <source>Thanks to Toubib for its work on Nevada map.</source>
        <translation type="vanished">Merci à Toubib pour son travail sur la carte Nevada</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="61"/>
        <source>Translators:</source>
        <translation>Traducteurs:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="72"/>
        <source>Language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AboutDlg.qml" line="73"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
</context>
<context>
    <name>AirportInfo</name>
    <message>
        <source>Id</source>
        <translation type="vanished">Id</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="vanished">Code</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Frequencies</source>
        <translation type="vanished">Fréquences</translation>
    </message>
    <message>
        <source>TACAN</source>
        <translation type="vanished">TACAN</translation>
    </message>
    <message>
        <source>ILS available on %1MHz</source>
        <translation type="vanished">ILS disponible sur %1MHz</translation>
    </message>
    <message>
        <source>No ILS</source>
        <translation type="vanished">Pas d&apos;ILS</translation>
    </message>
    <message>
        <source>Current controllers</source>
        <translation type="vanished">Controlleurs courant</translation>
    </message>
    <message>
        <source>Take approach</source>
        <translation type="vanished">Prendre l&apos;approche</translation>
    </message>
    <message>
        <source>No control available</source>
        <translation type="vanished">Pas de contrôle possible</translation>
    </message>
</context>
<context>
    <name>AirportProperties</name>
    <message>
        <source>Id</source>
        <translation type="vanished">Id</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="vanished">Code</translation>
    </message>
    <message>
        <source>Coalition</source>
        <translation type="vanished">Coalition</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>Altitude</source>
        <translation type="vanished">Altitude</translation>
    </message>
    <message>
        <source>QFE</source>
        <translation type="vanished">QFE</translation>
    </message>
    <message>
        <source>Declinaison</source>
        <translation type="vanished">Declinaison</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <source>Heading</source>
        <translation type="vanished">Orientation</translation>
    </message>
    <message>
        <source>Frequencies</source>
        <translation type="vanished">Fréquences</translation>
    </message>
    <message>
        <source>TACAN</source>
        <translation type="vanished">TACAN</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="107"/>
        <source>Open a chart view</source>
        <translation>Ouvrir la vue chart</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="119"/>
        <source>METAR : %1</source>
        <translation>METAR : %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="129"/>
        <source>Create ATIS on:</source>
        <translation>Créer l&apos;ATIS sur:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Take approach</source>
        <translation>Prendre l&apos;approche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="190"/>
        <source>Release approach</source>
        <translation>Lacher l&apos;approche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="223"/>
        <source>No control available</source>
        <translation>Pas de contrôle possible</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="236"/>
        <source>ILS available on %1MHz</source>
        <translation>ILS disponible sur %1MHz</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="239"/>
        <source>No ILS</source>
        <translation>Pas d&apos;ILS</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportProperties.qml" line="242"/>
        <source>Current controllers</source>
        <translation>Controlleurs courant</translation>
    </message>
</context>
<context>
    <name>AirportPropertiesModel</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="27"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="28"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="29"/>
        <source>Coalition</source>
        <translation>Coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="30"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="31"/>
        <source>Altitude</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="32"/>
        <source>QFE</source>
        <translation>QFE</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="33"/>
        <source>QNH</source>
        <translation>QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="34"/>
        <source>Declinaison</source>
        <translation>Déclinaison</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="vanished">Longueur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="35"/>
        <source>Heading</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="36"/>
        <source>Frequencies</source>
        <translation>Fréquences</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/AirportPropertiesModel.qml" line="38"/>
        <source>TACAN</source>
        <translation>TACAN</translation>
    </message>
</context>
<context>
    <name>BlockType</name>
    <message>
        <location filename="../lotatc/client/block/BlockType.cpp" line="19"/>
        <source>UNK</source>
        <comment>Short version of unknow</comment>
        <translation>UNK</translation>
    </message>
</context>
<context>
    <name>Braa</name>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="88"/>
        <source>H</source>
        <comment>Hot ex: 45/23Nm/1200ft/H</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="91"/>
        <source>FL</source>
        <comment>Flanking Left ex: 45/23Nm/1200ft/FL</comment>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="90"/>
        <source>C</source>
        <comment>Cold ex: 45/23Nm/1200ft/C</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../lotatc/client/Braa.cpp" line="89"/>
        <source>FR</source>
        <comment>Flanking Right ex: 45/23Nm/1200ft/FR</comment>
        <translation>FR</translation>
    </message>
</context>
<context>
    <name>BullseyeProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="45"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="62"/>
        <source>Bullseye properties</source>
        <translation>Propriétés du Bullseye</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="74"/>
        <source>Set coordinates</source>
        <translation>Editer coordonnées</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/BullseyeProperties.qml" line="83"/>
        <source>Restore from mission</source>
        <translation>Restaurer depuis la mission</translation>
    </message>
</context>
<context>
    <name>CarrierProperties</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="38"/>
        <source>Ship</source>
        <translation>Bateau</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/CarrierProperties.qml" line="39"/>
        <source>Airport</source>
        <translation>Aéroport</translation>
    </message>
</context>
<context>
    <name>ChatManager</name>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="22"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="18"/>
        <source>My side</source>
        <translation>Ma coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/chat/ChatManager.cpp" line="14"/>
        <source>LotAtc only</source>
        <translation>LotAtc seulement</translation>
    </message>
    <message>
        <source>Internal</source>
        <translation type="obsolete">Interne</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ChatMessage.qml" line="57"/>
        <source>Me:</source>
        <translation>Moi:</translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/ColorButton.qml" line="95"/>
        <source>Click to select, Shift+click to reset</source>
        <translation>Cliquer pour sélectionner, Maj+clic pour réinitialiser</translation>
    </message>
</context>
<context>
    <name>ConnectDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="129"/>
        <source>New bookmark</source>
        <translation>Nouveau signet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="136"/>
        <source>Save current bookmark</source>
        <translation>Sauver le signet courant</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="160"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="180"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="188"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="198"/>
        <source>Coalition:</source>
        <translation>Coalition:</translation>
    </message>
    <message>
        <source>blue</source>
        <translation type="obsolete">bleue</translation>
    </message>
    <message>
        <source>red</source>
        <translation type="obsolete">rouge</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="210"/>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="220"/>
        <source>Pseudo:</source>
        <translation>Pseudo:</translation>
    </message>
    <message>
        <source>Bookmark:</source>
        <translation type="vanished">Signets:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="171"/>
        <source>Enter a name...</source>
        <translation>Entrer un nom...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="167"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Bookmark current</source>
        <translation type="vanished">Ajouter le signet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="146"/>
        <source>Delete current bookmark</source>
        <translation>Effacer le signet courant</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="234"/>
        <source>Waiting maps...</source>
        <translation>Attente maps...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ConnectDlg.qml" line="245"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>CoordinatesDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="56"/>
        <source>Enter coordinates:</source>
        <translation>Entrer coordonnées:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/CoordinatesDialog.qml" line="63"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>DockMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Restore</source>
        <comment>Restore the window</comment>
        <translation>Restaurer la fenêtre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="61"/>
        <source>Maximize</source>
        <comment>Maximize the window</comment>
        <translation>Maximiser la fenêtre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="67"/>
        <source>Minimize</source>
        <comment>Minimize the window</comment>
        <translation>Minimiser la fenêtre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="72"/>
        <source>Make tab</source>
        <translation>Transformer un onglet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="81"/>
        <source>Make dialog</source>
        <translation>Transformer en une boite de dialogue</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="89"/>
        <source>Make dock</source>
        <translation>Transformer en dock</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="97"/>
        <source>Opacity</source>
        <translation>Opacité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/common/DockMenu.qml" line="118"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>HelpDlg</name>
    <message>
        <source>Bookmarks</source>
        <translation type="vanished">Signets</translation>
    </message>
</context>
<context>
    <name>LabelEdit</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="25"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="37"/>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="56"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/LabelEdit.qml" line="44"/>
        <source>Hovered</source>
        <translation>Survolé</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="60"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="63"/>
        <source>Connect to a server</source>
        <translation>Connexion au serveur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="68"/>
        <source>Disconnect</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="71"/>
        <source>Disconnect current session</source>
        <translation>Déconnecter la session courante</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="79"/>
        <source>Open options</source>
        <translation>Ouvrir options</translation>
    </message>
    <message>
        <source>See last news on LotAtc</source>
        <translation type="vanished">Voir les dernières nouvelles de LotAtc</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="121"/>
        <source>Enter/Exit fullscreen</source>
        <translation>Entrer/Sortir du plein écran</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="94"/>
        <source>Display online help</source>
        <translation>Afficher l&apos;aide en ligne</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="86"/>
        <source>New version available</source>
        <translation>Nouvelle version disponible</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="100"/>
        <source>Information on this software</source>
        <translation>Information sur ce logiciel</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="108"/>
        <source>Zoom out</source>
        <translation>Dézoomer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="115"/>
        <source>Zoom in</source>
        <translation>Zoomer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="127"/>
        <source>On top</source>
        <translation>Au-dessus</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="129"/>
        <source>Activate on top</source>
        <translation>Rester au-dessus</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="138"/>
        <source>Lock map scale/move</source>
        <translation>Verrouiller la carte mouvement/zoom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="147"/>
        <source>Show range rings</source>
        <translation>Afficher le cercle de distance</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="194"/>
        <source>Click to open %1</source>
        <translation>Cliquer pour ouvrir %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="232"/>
        <source>Change bearing mode</source>
        <translation>Changer le mode d&apos;affichage du cap</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="238"/>
        <source>Change units</source>
        <translation>Changer d&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="276"/>
        <source>Mission time, show/hide seconds</source>
        <translation>Temps de mission, afficher/cacher les secondes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="288"/>
        <source>Show controllers informations</source>
        <translation>Afficher les informations des controlleurs</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="296"/>
        <source>Show server settings</source>
        <translation>Afficher les réglages serveurs</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="83"/>
        <source>News</source>
        <translation>Nouvelles</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">A propos</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>METRIC</source>
        <translation>METRIQUE</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MainMenu.qml" line="236"/>
        <source>IMPERIAL</source>
        <translation>IMPERIAL</translation>
    </message>
</context>
<context>
    <name>MapItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItem.qml" line="224"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>MapItemProperties</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Inconnu</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="75"/>
        <source>Unknow</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="76"/>
        <source>TN: %1</source>
        <comment>Track Number of the unit</comment>
        <translation>TN: %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="91"/>
        <source>Group name</source>
        <translation>Nom du groupe</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="93"/>
        <source>Transponder</source>
        <translation>Transpondeur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="94"/>
        <source>Altitude</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="95"/>
        <source>Heading</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="96"/>
        <source>Ground Speed</source>
        <translation>Vitesse sol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="98"/>
        <source>Vertical Indicator</source>
        <translation>Indicateur vertical</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="99"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="100"/>
        <source>BullsEye</source>
        <translation>BullsEye</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="104"/>
        <source>Detection range</source>
        <translation>Distance de détection</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="106"/>
        <source>Threat range</source>
        <translation>Distance d&apos;attaque</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="139"/>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="152"/>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="245"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="171"/>
        <source>Side:</source>
        <translation>Côté:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="198"/>
        <source>Symbol:</source>
        <translation>Symbole:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="231"/>
        <source>Comment:</source>
        <comment>Comment on the unit</comment>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="272"/>
        <source>Message:</source>
        <translation>Message:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MapItemProperties.qml" line="285"/>
        <source>Text to send...</source>
        <translation>Message à envoyer...</translation>
    </message>
    <message>
        <source>Show on map</source>
        <translation type="vanished">Afficher sur la carte</translation>
    </message>
</context>
<context>
    <name>MyDockDialog</name>
    <message>
        <location filename="../lotatc/client/resources/qml/common/MyDockDialog.qml" line="62"/>
        <source>Waiting connection...</source>
        <translation>Attendre la connection...</translation>
    </message>
</context>
<context>
    <name>MyModels</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="48"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="49"/>
        <source>On</source>
        <translation>On</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="55"/>
        <source>Hide</source>
        <translation>Caché</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="56"/>
        <source>Show</source>
        <translation>Affiché</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="68"/>
        <source>Neutral</source>
        <translation>Neutre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="80"/>
        <source>Blue</source>
        <translation>Bleue</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="81"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="123"/>
        <source>Unknow</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="124"/>
        <source>Friend</source>
        <translation>Allié</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="125"/>
        <source>Enemy</source>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="140"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="141"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <source>Very Dark</source>
        <translation type="vanished">Très sombre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="183"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="184"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="185"/>
        <source>Deutsch</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="186"/>
        <source>Italian</source>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="187"/>
        <source>Polish</source>
        <translation>Polonais</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="188"/>
        <source>Czech</source>
        <translation>Tchèque</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="189"/>
        <source>Portuguese</source>
        <translation>Portuguais</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="190"/>
        <source>Serbian</source>
        <translation>Serbe</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="191"/>
        <source>Spanish</source>
        <translation>Espagnol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="192"/>
        <source>Chinese</source>
        <translation>Chinois</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="193"/>
        <source>Russian</source>
        <translation>Russe</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="194"/>
        <source>Swedish</source>
        <translation>Suédois</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="210"/>
        <source>Solid</source>
        <comment>Solid pattern brush</comment>
        <translation>Solide</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="211"/>
        <source>Dense</source>
        <comment>Dense pattern brush</comment>
        <translation>Dense</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="212"/>
        <source>Horizontal</source>
        <comment>Horizontal pattern brush</comment>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="213"/>
        <source>Vertical</source>
        <comment>Horizontal pattern brush</comment>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="214"/>
        <source>Diagonal</source>
        <comment>Diagonal pattern brush</comment>
        <translation>Diagonal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="215"/>
        <source>Cross</source>
        <comment>Cross pattern brush</comment>
        <translation>En croix</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="223"/>
        <source>Unit Name (or TN)</source>
        <translation>Nom de l&apos;unité (ou TN)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="224"/>
        <source>%(name|10) limit name to 10 characters</source>
        <translation>%(name|10) limité le nom à 10 caractères</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="228"/>
        <source>Group Name (if available)</source>
        <translation>Nom du groupe (si disponible)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="229"/>
        <source>%(group_name|10) limit name to 10 characters</source>
        <translation>%(group_name|10) limite le nombre de caractères à 10</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="233"/>
        <source>Unit type</source>
        <translation>Type d&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="238"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="239"/>
        <source>%(comment|10) limit comment to 10 characters</source>
        <translation>%(comment|10) limite le nombre de caractère à 10</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="243"/>
        <source>Unit heading</source>
        <translation>Orientation de l&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="432"/>
        <source>Show range rings</source>
        <translation>Afficher le cercle de distance</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="433"/>
        <source>Use bullseye as reference for measure line</source>
        <translation>Utiliser le bullseye comme référence pour la mesure</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="497"/>
        <source>Normal - Small font</source>
        <translation>Normal - Petite police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="498"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="499"/>
        <source>Normal - Large font</source>
        <translation>Normal - Grande police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="500"/>
        <source>Normal - Very large font</source>
        <translation>Normal - Très grande police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="501"/>
        <source>Dense - Small font</source>
        <translation>Dense - Petite police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="502"/>
        <source>Dense</source>
        <translation>Dense</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="503"/>
        <source>Dense - Large font</source>
        <translation>Dense - Grande police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="504"/>
        <source>Dense - Very large font</source>
        <translation>Dense - Très grande police</translation>
    </message>
    <message>
        <source>Use %(heading|mag) for magnetic deviation correction</source>
        <translation type="vanished">Utilisé %(heading|mag) pour la correction magnétique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="244"/>
        <source>Use %(heading|option) with option=true/mag/truemag for true/magnetic deviation correction (default is auto)</source>
        <translation>Utilise %(heading|option) avec option=true/mag/truemag pour la correction de la déviation magnétique vraie/magnétique (default est auto)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="248"/>
        <source>Altitude long format</source>
        <translation>Altitude format long</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="253"/>
        <source>Altitude short format</source>
        <translation>Altitude format court</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="258"/>
        <source>Ground speed</source>
        <translation>Vitesse sol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="263"/>
        <source>Ground speed in short format</source>
        <translation>Vitesse sol format court</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="268"/>
        <source>Ground speed unit</source>
        <translation>Unité de vitesse sol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="278"/>
        <source>Position to BullsEye</source>
        <translation>Position au BullsEye</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="283"/>
        <source>Vertical indicator</source>
        <translation>Indicateur de vitesse verticale</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="284"/>
        <source>%(vert_indic|nostable) to avoid stable cursor, %(vert_indic|n) with n=0/1/2 for different arrows</source>
        <translation>%(vert_indic|nostable) pour enlever la position stable, %(vert_indic|n) avec n=0/1/2 pour différents format de flèches</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="299"/>
        <source>Decimal</source>
        <translation>Décimale</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="300"/>
        <source>Longitude/Latitude</source>
        <translation>Longitude/Latitude</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="302"/>
        <source>MGRS</source>
        <translation>MGRS</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="303"/>
        <source>UTM</source>
        <translation>UTM</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="317"/>
        <source>AM</source>
        <comment>AM frequency band for radio</comment>
        <translation>AM</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="310"/>
        <source>UHF</source>
        <comment>UHF frequency band for radio</comment>
        <translation>UHF</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="142"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="195"/>
        <source>Brazilian Portuguese</source>
        <translation>Brésilien Portuguais</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="254"/>
        <source>%(alti_short|thousand) in imperial, show altitude in thousand feets</source>
        <translation>%(alti_short|thousand) en impérial, affiche l&apos;altitude en milliers de pieds</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="273"/>
        <source>Ground speed in Mach format</source>
        <translation>Vitesse sol en format Mach</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="288"/>
        <source>Transponder</source>
        <translation>Transpondeur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="301"/>
        <source>Longitude/Latitude Decimal</source>
        <translation>Longitude/Latitude décimale</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="309"/>
        <source>HF</source>
        <comment>HF frequency band for radio</comment>
        <translation>HF</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="311"/>
        <source>VHF</source>
        <comment>VHF frequency band for radio</comment>
        <translation>VHF</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="318"/>
        <source>FM</source>
        <comment>FM frequency band for radio</comment>
        <translation>FM</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="333"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="334"/>
        <source>OpenGL</source>
        <translation>OpenGL</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="335"/>
        <source>DirectX</source>
        <translation>DirectX</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="351"/>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="352"/>
        <source>Mid</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="353"/>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="369"/>
        <source>Disabled</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="370"/>
        <source>Low (4x)</source>
        <comment>antialiasing to 4x</comment>
        <translation>Bas (4x)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="371"/>
        <source>Mid (8x)</source>
        <comment>antialiasing to 8x</comment>
        <translation>Moyen (8x)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="372"/>
        <source>High (16x)</source>
        <comment>antialiasing to 16x</comment>
        <translation>Haut (16x)</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Accueil</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="389"/>
        <source>Labels</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="390"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="391"/>
        <source>Detection profiles</source>
        <translation>Profile de détection</translation>
    </message>
    <message>
        <source>Client FAQ</source>
        <translation type="vanished">FAQ du client</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="428"/>
        <source>Fullscreen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="429"/>
        <source>Show background label</source>
        <translation>Afficher le fond des étiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="430"/>
        <source>Show circle</source>
        <translation>Afficher cercle</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="431"/>
        <source>Change unit</source>
        <translation>Changer d&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>True</source>
        <comment>True bearing</comment>
        <translation>Vrai</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="448"/>
        <source>T</source>
        <comment>Shortcut for True bearing</comment>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>Magnetic</source>
        <comment>Magnetic bearing</comment>
        <translation>Magnétique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="449"/>
        <source>M</source>
        <comment>Shortcut for Magnetic bearing</comment>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>True/Magnetic</source>
        <comment>True/Magnetic bearing</comment>
        <translation>Vrai/Magnétique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="450"/>
        <source>T+M</source>
        <comment>Shortcut for True+Magnetic bearing</comment>
        <translation>V+M</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="475"/>
        <source>Plane</source>
        <translation>Avion</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="476"/>
        <source>Helicopter</source>
        <translation>Hélicoptère</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="477"/>
        <source>Ground moving</source>
        <translation>Unité au sol en mouvement</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="478"/>
        <source>Ground standing</source>
        <translation>Unité au sol fixe</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="479"/>
        <source>Ship</source>
        <translation>Bateau</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="480"/>
        <source>Sam</source>
        <translation>Sam</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="481"/>
        <source>Tank</source>
        <translation>Tank</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/MyModels.qml" line="482"/>
        <source>Airport</source>
        <translation>Aéroport</translation>
    </message>
</context>
<context>
    <name>MyWebView</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
</context>
<context>
    <name>NetDrawingText</name>
    <message>
        <location filename="../lotatc/common/NetDrawingText.cpp" line="55"/>
        <source>None</source>
        <translation>Vide</translation>
    </message>
</context>
<context>
    <name>NetObject</name>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="161"/>
        <source>You try to connect to an older incompatible version, server should be upgraded</source>
        <translation>Vous essayez de vous connecter à une ancienne version incompatible, le serveur doit être mis à jour</translation>
    </message>
    <message>
        <location filename="../lotatc/common/NetObject.cpp" line="163"/>
        <source>You try to connect to an newer incompatible version, client should be upgraded</source>
        <translation>Vous essayez de vous connecter à une nouvelle version incompatible, le client doit être mis à jour</translation>
    </message>
</context>
<context>
    <name>NewsDlg</name>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Chargement...</translation>
    </message>
    <message>
        <source>News</source>
        <translation type="vanished">Nouvelles</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>See on LotAtc website: </source>
        <translation type="obsolete">Voir sur le site de LotAtc:</translation>
    </message>
</context>
<context>
    <name>OptionsApproach</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="15"/>
        <source>Number of contact updates to display on glide</source>
        <translation>Nombre de mise à jour des contacts sur le glide</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsApproach.qml" line="32"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
</context>
<context>
    <name>OptionsChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="20"/>
        <source>Play sound on new messages</source>
        <translation>Jouer un son à la réception</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="26"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="30"/>
        <source>Background</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="35"/>
        <source>Please choose a color for background chat</source>
        <translation>Choisir une couleur pour le fond des messages</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="39"/>
        <source>Foreground</source>
        <translation>Avant-plan</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsChat.qml" line="44"/>
        <source>Please choose a color for foreground chat</source>
        <translation>Choisir une couleur pour le texte des messages</translation>
    </message>
</context>
<context>
    <name>OptionsDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="15"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="17"/>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="16"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="18"/>
        <source>Labels</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="19"/>
        <source>Approach</source>
        <translation>Approche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="20"/>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="obsolete">Choisir une langue (l&apos;application doit redémarrer):</translation>
    </message>
    <message>
        <source>Choose a map:</source>
        <translation type="obsolete">Choisir une carte:</translation>
    </message>
    <message>
        <source>Choose an unit (Key: Alt+N)</source>
        <comment>measurment: imperial or metric</comment>
        <translation type="obsolete">Choisir une unité (touche: Alt+N)</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="obsolete">Choisir un thème (l&apos;application doit redémarrer):</translation>
    </message>
    <message>
        <source>Choose a skin</source>
        <translation type="obsolete">Choisir un set d&apos;icônes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsDlg.qml" line="21"/>
        <source>Chat</source>
        <translation>Messagerie</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="obsolete">Jouer un son à la réception</translation>
    </message>
    <message>
        <source>Choose symbology</source>
        <translation type="obsolete">Choisir un set d&apos;icônes</translation>
    </message>
    <message>
        <source>Define color for friends</source>
        <translation type="obsolete">Définir la couleur pour les alliés</translation>
    </message>
    <message>
        <source>Choose color</source>
        <translation type="obsolete">Choisir la couleur</translation>
    </message>
    <message>
        <source>Please choose a color for friend unit</source>
        <translation type="obsolete">Choisissez une couleur pour les unités alliés</translation>
    </message>
    <message>
        <source>Default color</source>
        <translation type="obsolete">Couleur par défaut</translation>
    </message>
    <message>
        <source>Define color for enemies</source>
        <translation type="obsolete">Définir la couleur pour les ennemies</translation>
    </message>
    <message>
        <source>Please choose a color for enemies unit</source>
        <translation type="obsolete">Choisissez une couleur pour les unités ennemies</translation>
    </message>
    <message>
        <source>Define size of Item</source>
        <translation type="obsolete">Définir la taille des items</translation>
    </message>
    <message>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation type="obsolete">Définir la longueur du vecteur vitesse (0 pour taille relative à la vitesse)</translation>
    </message>
    <message>
        <source>%1 seconds</source>
        <translation type="obsolete">%1 secondes</translation>
    </message>
    <message>
        <source>Zoom independent</source>
        <translation type="obsolete">Taille fixe relative à la vitesse uniquement</translation>
    </message>
    <message>
        <source>Define labels for friends</source>
        <translation type="obsolete">Définir les étiquettes pour les alliés</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="obsolete">Définir les étiquettes pour les ennemies</translation>
    </message>
    <message>
        <source>Show background for label (Key: Alt+L)</source>
        <translation type="obsolete">Afficher le fond des étiquettes (touche: Alt+L)</translation>
    </message>
    <message>
        <source>Size of font label:</source>
        <translation type="obsolete">Taille de la police:</translation>
    </message>
    <message>
        <source>Available values:</source>
        <translation type="obsolete">Labels disponibles:</translation>
    </message>
    <message>
        <source>Unit Name (or TN)</source>
        <translation type="obsolete">Nom de l&apos;unité (ou TN)</translation>
    </message>
    <message>
        <source>Unit type</source>
        <translation type="obsolete">Type d&apos;unité</translation>
    </message>
    <message>
        <source>Altitude long format</source>
        <translation type="obsolete">Altitude format long</translation>
    </message>
    <message>
        <source>Altitude short format</source>
        <translation type="obsolete">Altitude format court</translation>
    </message>
    <message>
        <source>Ground speed</source>
        <translation type="obsolete">Vitesse sol</translation>
    </message>
    <message>
        <source>Ground speed in short format</source>
        <translation type="obsolete">Vitesse sol format court</translation>
    </message>
    <message>
        <source>Ground speed unit</source>
        <translation type="obsolete">Unité de vitesse sol</translation>
    </message>
    <message>
        <source>Position to BullsEye</source>
        <translation type="obsolete">Position au BullsEye</translation>
    </message>
    <message>
        <source>Vertical indicator</source>
        <translation type="obsolete">Indicateur de vitesse verticale</translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="obsolete">Plus d&apos;infos ici</translation>
    </message>
    <message>
        <source>Choose a unit (Key: U)</source>
        <translation type="obsolete">Choisir une unité (touche U)</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="obsolete">Luminosité</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">Défault</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="obsolete">Contraste</translation>
    </message>
    <message>
        <source>Define Labels for friends</source>
        <translation type="obsolete">Définir les étiquettes pour les alliés</translation>
    </message>
    <message>
        <source>Define Labels for ennemies</source>
        <translation type="obsolete">Définir les étiquettes pour les ennemies</translation>
    </message>
    <message>
        <source>Show background for label (Key: L)</source>
        <translation type="obsolete">Afficher le fond des étiquettes (touche L)</translation>
    </message>
</context>
<context>
    <name>OptionsGeneral</name>
    <message>
        <source>Choose a language (need application restart):</source>
        <translation type="obsolete">Choisir une langue (l&apos;application doit redémarrer):</translation>
    </message>
    <message>
        <source>Choose a theme for the application (need application restart):</source>
        <translation type="obsolete">Choisir un thème (l&apos;application doit redémarrer):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="27"/>
        <source>Choose a language:</source>
        <translation>Choisir une langue:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="83"/>
        <source>Choose a theme for the application:</source>
        <translation>Choisir un thème pour l&apos;application:</translation>
    </message>
    <message>
        <source>Play sound on new messages</source>
        <translation type="vanished">Jouer un son à la réception</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="134"/>
        <source>Performances</source>
        <translation>Performances</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="44"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="68"/>
        <source>Choose a variant for the UI (size of the UI):</source>
        <translation>Choisir une variante pour l&apos;interface (taille de l&apos;interface):</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="98"/>
        <source>Choose a variation color for the application:</source>
        <translation>Choisir une couleur de variation pour l&apos;application:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="127"/>
        <source>Check update at start</source>
        <translation>Vérifier les mises à jour au démarrage</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="140"/>
        <source>Graphical effects:</source>
        <translation>Effets graphiques:</translation>
    </message>
    <message>
        <source>Change graphic engine [current=%1]:</source>
        <comment>The %1 will be replacing by current engine: auto,opengl, directx...</comment>
        <translation type="vanished">Changer le moteur graphique [courant=%1]:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="160"/>
        <source>Antialiasing:</source>
        <translation>Antialiasing:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="113"/>
        <source>Restore default dock and windows positions</source>
        <translation>Restaurer la position des docks et fenêtres</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="172"/>
        <source>(*) : Need application restart</source>
        <translation>(*): l&apos;application doit être redémarrée</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsGeneral.qml" line="115"/>
        <source>Click to reset</source>
        <translation>Cliquer pour restaurer</translation>
    </message>
</context>
<context>
    <name>OptionsItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="66"/>
        <source>Choose symbology</source>
        <translation>Choisir un set d&apos;icônes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="27"/>
        <source>Define color for items</source>
        <translation>Définir la couleur pour les unités</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="32"/>
        <source>Friends</source>
        <translation>Alliés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="36"/>
        <source>Please choose a color for friends unit</source>
        <translation>Choisissez une couleur pour les unités alliés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="41"/>
        <source>Enemies</source>
        <translation>Ennemis</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="45"/>
        <source>Please choose a color for enemies unit</source>
        <translation>Choisissez une couleur pour les unités ennemies</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="49"/>
        <source>Neutral</source>
        <translation>Neutre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="53"/>
        <source>Please choose a color for neutral unit</source>
        <translation>Choisissez une couleur pour les unités neutres</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="62"/>
        <source>Items parameters</source>
        <translation>Paramètres des pistes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="77"/>
        <source>Define size of Item</source>
        <translation>Définir la taille des items</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="127"/>
        <source>Define opacity of circles</source>
        <translation>Définir l&apos;opacité des cercles</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="132"/>
        <source>Detection:</source>
        <translation>Détection:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="147"/>
        <source>Threat:</source>
        <translation>Attaque:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="90"/>
        <source>Define speed vector mode (put at 0s for zoom independent)</source>
        <translation>Définir la longueur du vecteur vitesse (0 pour taille relative à la vitesse)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>Zoom independent</source>
        <translation>Taille fixe relative à la vitesse uniquement</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="108"/>
        <source>%1 seconds</source>
        <translation>%1 secondes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsItem.qml" line="111"/>
        <source>Define number of ghost</source>
        <translation>Nombre de fantômes</translation>
    </message>
</context>
<context>
    <name>OptionsLabel</name>
    <message>
        <source>Define labels for friends</source>
        <translation type="vanished">Définir les étiquettes pour les alliés</translation>
    </message>
    <message>
        <source>Define labels for enemies</source>
        <translation type="vanished">Définir les étiquettes pour les ennemies</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="19"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="23"/>
        <source>Show background for label (Key: Alt+L)</source>
        <translation>Afficher le fond des étiquettes (touche: Alt+L)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="29"/>
        <source>Size of font label:</source>
        <translation>Taille de la police:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="40"/>
        <source>Background:</source>
        <translation>Fond:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="44"/>
        <source>Please choose a color for label background</source>
        <translation>Choisir une couleur pour le fond de l&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="48"/>
        <source>Foreground:</source>
        <translation>Avant-plan:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="52"/>
        <source>Please choose a color for label foreground</source>
        <translation>Choisir une couleur pour l&apos;avant-plan de l&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="56"/>
        <source>Background mode alert 1:</source>
        <translation>Fond du mode alerte 1:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="60"/>
        <source>Please choose a color for label background in simple alert</source>
        <translation>Choisir une couleur pour le fond de l&apos;étiquette en alerte simple</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="64"/>
        <source>Background mode alert 2:</source>
        <translation>Fond du mode alerte 2:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="68"/>
        <source>Please choose a color for label background in alert</source>
        <translation>Choisir une couleur pour le fond de l&apos;étiquette en alerte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="76"/>
        <source>Define contents for labels</source>
        <translation>Définir le contenu des étiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="79"/>
        <source>Friend</source>
        <translation>Allié</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="80"/>
        <source>Others</source>
        <translation>Autres</translation>
    </message>
    <message>
        <source>Enemies</source>
        <translation type="obsolete">Ennemis</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="126"/>
        <source>Available values:</source>
        <translation>Labels disponibles:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsLabel.qml" line="138"/>
        <source>Full documentation</source>
        <translation>Documentation complète</translation>
    </message>
    <message>
        <source>Click here to access to full documentation</source>
        <translation type="vanished">Plus d&apos;infos ici</translation>
    </message>
</context>
<context>
    <name>OptionsMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="20"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="24"/>
        <source>Land</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="29"/>
        <source>Please choose a color for land</source>
        <translation>Choisir une couleur pour le terrain</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="33"/>
        <source>Sea</source>
        <translation>Mer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="38"/>
        <source>Please choose a color for sea</source>
        <translation>Choisir une couleur pour la mer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="42"/>
        <source>Range rings</source>
        <translation>Cercle de distance</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="54"/>
        <source>Please choose a color for range rings</source>
        <translation>Choisissez une couleur pour le cercle des distances</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="58"/>
        <source>Measure line</source>
        <translation>Ligne de mesure</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="63"/>
        <source>Please choose a color for measure line</source>
        <translation>Choisissez une couleur pour la ligne de mesure</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="67"/>
        <source>Measure line background</source>
        <translation>Fond de la ligne de mesure</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="72"/>
        <source>Please choose a color for background measure line label</source>
        <translation>Choisir une couleur pour le fond de la ligne de mesure</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="76"/>
        <source>BRAA line</source>
        <translation>Ligne BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="81"/>
        <source>Please choose a color for braa</source>
        <translation>Choisir une couleur pour la ligne BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="88"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="94"/>
        <source>Brightness</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="112"/>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="135"/>
        <source>Default</source>
        <translation>Défault</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsMap.qml" line="116"/>
        <source>Contrast</source>
        <translation>Contraste</translation>
    </message>
</context>
<context>
    <name>OptionsShortcut</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="30"/>
        <source>You can change shortcuts here, just double-clic on it to change it.</source>
        <translation>Vous pouvez changer les raccourcis ici, double-clic pour changer.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="61"/>
        <source>Click on a shortcut to edit it</source>
        <translation>Cliquer sur un raccourcis pour l&apos;éditer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="70"/>
        <source>Save</source>
        <translation>Sauvegardé</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="vanished">Raccourci</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Editer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/OptionsShortcut.qml" line="75"/>
        <source>Restore</source>
        <translation>Restaurer</translation>
    </message>
</context>
<context>
    <name>PageAirportView</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageAirportView.qml" line="12"/>
        <source>Airport view</source>
        <translation>Vue aéroport</translation>
    </message>
</context>
<context>
    <name>PageBraa</name>
    <message>
        <source>Show BRAA on map:</source>
        <translation type="vanished">Afficher BRAA sur la carte:</translation>
    </message>
    <message>
        <source>Show BRAA text:</source>
        <translation type="vanished">Afficher le texte du BRAA:</translation>
    </message>
    <message>
        <source>List of all BRAA in use</source>
        <translation type="vanished">Liste des BRAA courants</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="52"/>
        <source>Show</source>
        <translation>Affiché</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="90"/>
        <source>SendIt</source>
        <translation>Envoi</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="144"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="170"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="145"/>
        <source>Target</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="186"/>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <source>Show BRAA interception:</source>
        <translation type="vanished">Afficher la ligne d&apos;interception avec le BRAA:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="279"/>
        <source>Send to pilot every:</source>
        <translation>Envoyer au pilote toutes les:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="274"/>
        <source>Send to pilot with unit</source>
        <translation>Envoyer au pilote avec l&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="13"/>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="146"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="24"/>
        <source>list</source>
        <translation>List</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="25"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="149"/>
        <source>Interception</source>
        <translation>Interception</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>METRIC</source>
        <translation>METRIQUE</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="175"/>
        <source>IMPERIAL</source>
        <translation>IMPERIAL</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="216"/>
        <source>Delete selected</source>
        <translation>Effacer la sélection</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="234"/>
        <source>Delete all</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="253"/>
        <source>Show BRAA on map</source>
        <translation>Afficher les BRAA sur la carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="259"/>
        <source>Show BRAA text</source>
        <translation>Afficher le texte des BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageBraa.qml" line="266"/>
        <source>Show BRAA interception</source>
        <translation>Afficher les informations d&apos;interception pour les BRAA</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Effacer</translation>
    </message>
</context>
<context>
    <name>PageChart</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChart.qml" line="14"/>
        <source>Chart view for %1</source>
        <comment>%1 replaced by airport name</comment>
        <translation>Vue de la chart de %1</translation>
    </message>
</context>
<context>
    <name>PageChat</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="12"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Current channel:</source>
        <translation type="vanished">Canal courant:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageChat.qml" line="64"/>
        <source>Text to send...</source>
        <translation>Message à envoyer...</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="obsolete">Envoyer</translation>
    </message>
</context>
<context>
    <name>PageDraw</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="14"/>
        <source>Draw</source>
        <translation>Dessin</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="95"/>
        <source>Please choose a font</source>
        <translation>Choisir une police</translation>
    </message>
    <message>
        <source>Show draw</source>
        <translation type="vanished">Afficher les dessins</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="169"/>
        <source>Edit Mode</source>
        <translation>Mode édition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="185"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="254"/>
        <source>Delete All</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="256"/>
        <source>All private draws</source>
        <translation>Tous les dessins privés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="260"/>
        <source>All shared draws (WARNING)</source>
        <translation>Tous les dessins partagés (ATTENTION)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="268"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="195"/>
        <source>New draw</source>
        <translation>Nouveau dessin</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="110"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="126"/>
        <source>Please choose a file</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="177"/>
        <source>Add text on coordinates</source>
        <translation>Ajouter du texte sur les coordonnées</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="210"/>
        <source>Shared</source>
        <translation>Partagé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="212"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="235"/>
        <source>Circle</source>
        <translation>Cercle</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="217"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="239"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="222"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="243"/>
        <source>Polygon</source>
        <translation>Polygone</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="227"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="247"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="233"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="365"/>
        <source>Private</source>
        <translation>Privé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="279"/>
        <source>Save to file...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="283"/>
        <source>Load from file...</source>
        <translation>Charger depuis...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="292"/>
        <source>Select a draw:</source>
        <translation>Sélectionner un dessin:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="336"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="344"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="349"/>
        <source>Enter name</source>
        <translation>Entrer nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="357"/>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="373"/>
        <source>Make it public:</source>
        <translation>Partagé:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="386"/>
        <source>Share it</source>
        <translation>Partagé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="394"/>
        <source>Text:</source>
        <translation>Texte:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="400"/>
        <source>Enter text to display</source>
        <translation>Texte à afficher</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="410"/>
        <source>Font:</source>
        <translation>Police:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="416"/>
        <source>Choose font</source>
        <translation>Choisir une police</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="426"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="431"/>
        <source>Foreground color</source>
        <translation>Couleur de premier-plan</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="442"/>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="448"/>
        <source>Background color</source>
        <translation>Couleur d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="458"/>
        <source>Line width</source>
        <translation>Epaisseur de trait</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageDraw.qml" line="479"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
</context>
<context>
    <name>PageGraph</name>
    <message>
        <source>Glide %1</source>
        <translation type="vanished">Glide %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="228"/>
        <source>Please choose a file</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="25"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="669"/>
        <source>Release approach</source>
        <translation>Lacher l&apos;approche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="27"/>
        <source>Save graph</source>
        <translation>Sauvegarder le graphique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="18"/>
        <source>Approach %1</source>
        <translation>Approche de %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="294"/>
        <source>Glidepath</source>
        <translation>Pente</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="496"/>
        <source>Track</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="555"/>
        <source>Items</source>
        <translation>Pistes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="556"/>
        <source>Airport</source>
        <translation>Aéroport</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="557"/>
        <source>Settings</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="689"/>
        <source>Show labels</source>
        <translation>Afficher les étiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="697"/>
        <source>Show circles on map</source>
        <translation>Afficher les cercles sur la carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="708"/>
        <source>Steady level:</source>
        <translation>Maxima:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="726"/>
        <source>Minima:</source>
        <translation>Minima:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="744"/>
        <source>Glide angle:</source>
        <translation>Angle de la pente:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="774"/>
        <source>LSLLC:</source>
        <translation>LSLLC:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="591"/>
        <source>Show</source>
        <translation>Affiché</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="640"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageGraph.qml" line="649"/>
        <source>Distance(%1)</source>
        <comment>%1 will be replaced by unit: km</comment>
        <translation>Distance(%1)</translation>
    </message>
    <message>
        <source>Glide</source>
        <translation type="vanished">Pente</translation>
    </message>
    <message>
        <source>Azimuth</source>
        <translation type="vanished">Azimuth</translation>
    </message>
    <message>
        <source>Glide slope</source>
        <translation type="vanished">Pente</translation>
    </message>
    <message>
        <source>Localizer</source>
        <translation type="vanished">Localizer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Effacer</translation>
    </message>
</context>
<context>
    <name>PageMap</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="51"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Choose an unit</source>
        <comment>measurment: imperial or metric</comment>
        <translation type="obsolete">Choisir une unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="50"/>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="77"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Couleurs</translation>
    </message>
    <message>
        <source>Land</source>
        <translation type="vanished">Sol</translation>
    </message>
    <message>
        <source>Sea</source>
        <translation type="vanished">Mer</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Avancé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="114"/>
        <source>Altitude filter</source>
        <translation>Filtre d&apos;altitude</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="257"/>
        <source>Show airport code instead of name</source>
        <translation>Afficher le code de l&apos;aéroport au lieu de son nom</translation>
    </message>
    <message>
        <source>Choose an unit (Key: Alt+N)</source>
        <comment>measurment: imperial or metric</comment>
        <translation type="obsolete">Choisir une unité (touche: Alt+N)</translation>
    </message>
    <message>
        <source>from</source>
        <translation type="obsolete">de</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="obsolete">à</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="153"/>
        <source>Speed filter</source>
        <translation>Filtre de vitesse</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="14"/>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="49"/>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="63"/>
        <source>Choose a map:</source>
        <translation>Choisir une carte:</translation>
    </message>
    <message>
        <source>Layers:</source>
        <translation type="vanished">Couches:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="100"/>
        <source>Please choose a color for layer</source>
        <translation>Choisir une couleur pour la couche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="191"/>
        <source>Hide ground units</source>
        <translation>Cacher les unités sol</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="215"/>
        <source>Show draw</source>
        <translation>Afficher les dessins</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="221"/>
        <source>Show measure line value on bottom of the window</source>
        <translation>Afficher les valeurs de la ligne de mesure en bas de la fenêtre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="237"/>
        <source>Coordinates format</source>
        <translation>Format de coodonées</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageMap.qml" line="251"/>
        <source>Example: %1</source>
        <translation>Exemple: %1</translation>
    </message>
    <message>
        <source>Land color</source>
        <translation type="vanished">Couleur de terrain</translation>
    </message>
    <message>
        <source>Please choose a color for land</source>
        <translation type="vanished">Choisir une couleur pour le terrain</translation>
    </message>
    <message>
        <source>Sea color</source>
        <translation type="vanished">Couleur de la mer</translation>
    </message>
    <message>
        <source>Please choose a color for sea</source>
        <translation type="vanished">Choisir une couleur pour la mer</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="vanished">Luminosité</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Défault</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="vanished">Contraste</translation>
    </message>
</context>
<context>
    <name>PageObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="15"/>
        <source>Objects</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="34"/>
        <source>Enter text to filter...</source>
        <translation>Entrer du texte pour filtrer...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="52"/>
        <source>Coalition</source>
        <translation>Coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="69"/>
        <source>Category</source>
        <translation>Categorie</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageObjects.qml" line="91"/>
        <source>Human</source>
        <translation>Humain</translation>
    </message>
</context>
<context>
    <name>PageProperties</name>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propriétés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Properties %1</source>
        <translation>Propriétés de %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="17"/>
        <source>Selection&apos;s properties</source>
        <translation>Propriétes de la sélection</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="24"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="26"/>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="120"/>
        <source>Pin</source>
        <translation>Epingler</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="34"/>
        <source>Make this object as new bullseye reference?</source>
        <translation>Faire de cet objet comme nouveau bullseye?</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="35"/>
        <source>If yes, bullseye will be set to this object (only for you)</source>
        <translation>Si oui, cet objet deviendra le nouveau bullseye (seulement pour vous)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="98"/>
        <source>Select an item/airport to see its properties</source>
        <translation>Sélectionner un object pour voir ses propriétés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Set current virtual position at this object</source>
        <translation>Définir la position virtuelle sur cet objet</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="134"/>
        <source>Need radio enabled and an active radar unit</source>
        <translation>Nécessite que la radio soit active et que l&apos;unité soit un radar actif</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="148"/>
        <source>Display range rings at this object</source>
        <translation>Afficher le cercle des distances</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="156"/>
        <source>Set this object as bullseye reference</source>
        <translation>Faire de cet objet comme nouveau bullseye</translation>
    </message>
    <message>
        <source>Name: %1</source>
        <comment>Name of the unit</comment>
        <translation type="obsolete">Nom: %1</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Inconnu</translation>
    </message>
    <message>
        <source>Id: %1</source>
        <comment>Id of the unit</comment>
        <translation type="obsolete">Id: %1</translation>
    </message>
    <message>
        <source>TN: %1</source>
        <comment>Track Number of the unit</comment>
        <translation type="vanished">TN: %1</translation>
    </message>
    <message>
        <source>Altitude</source>
        <translation type="vanished">Altitude</translation>
    </message>
    <message>
        <source>Heading</source>
        <translation type="vanished">Orientation</translation>
    </message>
    <message>
        <source>Ground Speed</source>
        <translation type="vanished">Vitesse sol</translation>
    </message>
    <message>
        <source>Vertical Indicator</source>
        <translation type="vanished">Indicateur vertical</translation>
    </message>
    <message>
        <source>Position</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>Select a contact to see its properties</source>
        <translation type="vanished">Sélectionner un contact pour voir ses propriétés</translation>
    </message>
    <message>
        <source>Name:</source>
        <comment>Name of the unit</comment>
        <translation type="vanished">Nom:</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Appliquer</translation>
    </message>
    <message>
        <source>Symbol:</source>
        <translation type="vanished">Symbole:</translation>
    </message>
    <message>
        <source>Message:</source>
        <translation type="vanished">Message:</translation>
    </message>
    <message>
        <source>Text to send...</source>
        <translation type="vanished">Message à envoyer...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageProperties.qml" line="125"/>
        <source>Show on map</source>
        <translation>Afficher sur la carte</translation>
    </message>
</context>
<context>
    <name>PageRadio</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="12"/>
        <source>Radio</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="27"/>
        <source>Airport</source>
        <translation>Aéroport</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="40"/>
        <source>Radar</source>
        <translation>Radar</translation>
    </message>
    <message>
        <source>Radios</source>
        <translation type="vanished">Radios</translation>
    </message>
    <message>
        <source>Location not defined</source>
        <translation type="vanished">Position non définie</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="109"/>
        <source>Location</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>Current Location:%1</source>
        <translation>Position courante:%1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="148"/>
        <source>NOT DEFINED</source>
        <translation>NON DEFINIE</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="167"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="168"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="174"/>
        <source>Refresh list (not automatic)</source>
        <translation>Rafraichir la liste (non automatique)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="179"/>
        <source>Set my position to selected one</source>
        <translation>Définir ma position sur celle sélectionée</translation>
    </message>
    <message>
        <source>currently only support &lt;a href=&apos;http://tacnoworld.fr/UniversRadio/&apos;&gt;Universal Radio project&lt;/a&gt;&lt;br /&gt;This project is currently in Beta phase and made by Tacno from &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;.</source>
        <translation type="vanished">Support seulement  &lt;a href=&apos;http://tacnoworld.fr/UniversRadio/&apos;&gt;Universal Radio project&lt;/a&gt;&lt;br /&gt;Ce projet est actuellement en phase Beta et réalisé par Tacno de la &lt;a href=&apos;http://www.3rd-wing.net&apos;&gt;3rd-wing&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageRadio.qml" line="75"/>
        <source>Radio enabled</source>
        <translation>Activer la radio</translation>
    </message>
</context>
<context>
    <name>PageWeather</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="176"/>
        <source>Sky clear</source>
        <translation>Ciel clair</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="177"/>
        <source>Some clouds</source>
        <translation>Quelques nuages</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="178"/>
        <source>Clouds</source>
        <translation>Nuageux</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="179"/>
        <source>Overcast</source>
        <translation>Couvert</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="180"/>
        <source>Rain</source>
        <translation>Pluie</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="181"/>
        <source>Snow</source>
        <translation>Neige</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="182"/>
        <source>Thunderstorms</source>
        <translation>Orages</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="183"/>
        <source>Snow thunderstorms</source>
        <translation>Orages de neige</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="211"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="212"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="226"/>
        <source>Distance in %1</source>
        <comment>%1 is unit</comment>
        <translation>Distance en %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="233"/>
        <source>From</source>
        <comment>Wind direction</comment>
        <translation>Du</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="vanished">Direction</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="240"/>
        <source>Speed in %1</source>
        <comment>%1 is unit</comment>
        <translation>Vitesse en %1</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="254"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="255"/>
        <source>Tasks</source>
        <translation>Objectifs</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="11"/>
        <source>Weather</source>
        <translation>Météo</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="142"/>
        <source>METAR</source>
        <translation>METAR</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="143"/>
        <source>Visibility</source>
        <translation>Visibilité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="144"/>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="145"/>
        <source>Thickness</source>
        <translation>Epaisseur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="147"/>
        <source>QNH</source>
        <translation>QNH</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="148"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="218"/>
        <source>Wind from</source>
        <translation>Vent depuis</translation>
    </message>
    <message>
        <source>0m</source>
        <translation type="obsolete">0m</translation>
    </message>
    <message>
        <source>2000m</source>
        <translation type="obsolete">2000m</translation>
    </message>
    <message>
        <source>8000m</source>
        <translation type="obsolete">8000m</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/PageWeather.qml" line="284"/>
        <source>Waiting for mission datas...</source>
        <translation>Attente des données de mission...</translation>
    </message>
</context>
<context>
    <name>ProfileDlg</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>For all</source>
        <translation>Pour tous</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Only for friends</source>
        <translation>Seulement les alliés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="23"/>
        <source>Disable</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="25"/>
        <source>See all (no radar management)</source>
        <translation>Voir tout (pas de gestion de radar)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="26"/>
        <source>Cylinder (no altitude management)</source>
        <translation>Cylindrique (pas de gestion d&apos;altitude)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="27"/>
        <source>Spherical (altitude is used)</source>
        <translation>Sphérique (altitude pris en compte)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="28"/>
        <source>Conical (like spherical but with real constraints)</source>
        <translation>Conique (comme sphérique mais avec des contraintes réelles)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="30"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="31"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Disabled</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>Only coalition</source>
        <translation>Seulement la coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="32"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="34"/>
        <source>Profile</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="36"/>
        <source>Theater</source>
        <translation>Theatre</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="38"/>
        <source>Show enemies parameters (name, type,...)</source>
        <translation>Afficher les paramètres des ennemies (nom, type,...)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="40"/>
        <source>Show enemies SAM/Ships when out of radar</source>
        <translation>Afficher les SAM/bateaux ennemies hors couverture radar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="42"/>
        <source>Low speed airplanes are filtered</source>
        <translation>Avions avec basse vitesse filtrés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="44"/>
        <source>Use relief</source>
        <translation>Utiliser le relief</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="46"/>
        <source>Automatic type for new item</source>
        <translation>Type automatique pour les nouveaux contacts</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="48"/>
        <source>Radar modelisation</source>
        <translation>Modelisation radar</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="50"/>
        <source>Approach</source>
        <translation>Approche</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="52"/>
        <source>Virtual awacs name (nothing=desactivated)</source>
        <translation>nom des AWACS virtuel (rien=désactiver)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="54"/>
        <source>Virtual awacs range</source>
        <translation>Portée des AWACS virtuels</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="56"/>
        <source>Minimal detection limit</source>
        <translation>Distance de détection minimale</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="58"/>
        <source>Force player name</source>
        <translation>Forcer le nom du joueur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="60"/>
        <source>Neutral coalition by default</source>
        <translation>Coalition neutre par défaut</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="67"/>
        <source>Server parameters</source>
        <translation>Paramètres serveur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="72"/>
        <source>Name</source>
        <comment>Name of server parameter</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ProfileDlg.qml" line="73"/>
        <source>Value</source>
        <comment>Value for the server parameter</comment>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>RadioManager</name>
    <message>
        <source>Radio 1</source>
        <comment>Name of the radio 1</comment>
        <translation type="vanished">Radio 1</translation>
    </message>
    <message>
        <source>Radio 2</source>
        <comment>Name of the radio 2</comment>
        <translation type="vanished">Radio 2</translation>
    </message>
    <message>
        <source>Radio 3</source>
        <comment>Name of the radio 3</comment>
        <translation type="vanished">Radio 3</translation>
    </message>
    <message>
        <location filename="../lotatc/client/radio/RadioManager.cpp" line="10"/>
        <source>Radio %1</source>
        <comment>Name of the radio n</comment>
        <translation>Radio %1</translation>
    </message>
</context>
<context>
    <name>ServerInfo</name>
    <message>
        <source>blue</source>
        <comment>coalition blue</comment>
        <translation type="vanished">bleue</translation>
    </message>
    <message>
        <source>red</source>
        <comment>coalition red</comment>
        <translation type="vanished">rouge</translation>
    </message>
</context>
<context>
    <name>ServerModel</name>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="116"/>
        <source>New</source>
        <comment>New bookmark</comment>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="151"/>
        <source>Default Blue</source>
        <translation>Par défaut Blue</translation>
    </message>
    <message>
        <location filename="../lotatc/client/ServerModel.cpp" line="153"/>
        <source>Default Red</source>
        <translation>Par défaut Red</translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>There is something wrong with your license. Please contact support.</source>
        <translation>Problème avec votre license, contacter le support.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="40"/>
        <source>Not registered&lt;br&gt;Put your %1 in the &lt;u&gt;%2 directory&lt;/u&gt;</source>
        <translation>Non enregistré&lt;br&gt;Mettre votre fichier %1 dans le répertoire &lt;u&gt;%2&lt;/u&gt;</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/Splash.qml" line="51"/>
        <source>If you do not have a license key &lt;u&gt;click HERE&lt;/u&gt; to buy one!</source>
        <translation>Si vous n&apos;avez pas de licence &lt;u&gt;Cliquer ICI&lt;/u&gt; pour en acheter une!</translation>
    </message>
</context>
<context>
    <name>TableObjects</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="51"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="54"/>
        <source>Category</source>
        <translation>Categorie</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="59"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="62"/>
        <source>Coalition</source>
        <translation>Coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="73"/>
        <source>Human</source>
        <translation>Humain</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="80"/>
        <source>Detection range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation>Distance de détection (%1)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="87"/>
        <source>Threat range (%1)</source>
        <comment>%1 is the unit</comment>
        <translation>Distance d&apos;attaque (%1)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TableObjects.qml" line="94"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
</context>
<context>
    <name>ToolsSideBar</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="16"/>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="23"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="30"/>
        <source>Weather</source>
        <translation>Météo</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="38"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/ToolsSideBar.qml" line="46"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
</context>
<context>
    <name>TrollerItem</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerItem.qml" line="29"/>
        <source>Airports:</source>
        <translation>Aéroports:</translation>
    </message>
</context>
<context>
    <name>TrollerList</name>
    <message>
        <source>Airport</source>
        <translation type="vanished">Aéroport</translation>
    </message>
    <message>
        <source>Radar</source>
        <translation type="vanished">Radar</translation>
    </message>
    <message>
        <source>My settings</source>
        <translation type="vanished">Mes paramètres</translation>
    </message>
    <message>
        <source>Choose my position</source>
        <translation type="vanished">Choisir ma position</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="11"/>
        <source>Coalition trollers</source>
        <translation>Controlleurs de la coalition</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/TrollerList.qml" line="24"/>
        <source>List of airports</source>
        <translation>Liste des aéroports</translation>
    </message>
</context>
<context>
    <name>Unit</name>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="12"/>
        <source>Metric</source>
        <translation>Métrique</translation>
    </message>
    <message>
        <location filename="../lotatc/common/Unit.cpp" line="13"/>
        <source>Imperial</source>
        <translation>Impérial</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>There is something wrong with your license. Please contact support.</source>
        <translation type="obsolete">Problème avec votre license, contacter le support.</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="12"/>
        <source>(Not registered)</source>
        <translation>(Non enregistré)</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="268"/>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="275"/>
        <source>Connection and authentification OK</source>
        <translation>Connexion et authentification OK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="284"/>
        <source>Connection and authentification error: </source>
        <translation>Erreur de connexion ou d&apos;authentification:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="306"/>
        <source>New mission received, load data from server</source>
        <translation>Nouvelle mission reçue, chargement des données depuis le serveur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="950"/>
        <source>Show range rings</source>
        <translation>Afficher le cercle de distance</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="956"/>
        <source>Use bullseye as reference for measure line</source>
        <translation>Utiliser le bullseye comme référence pour la mesure</translation>
    </message>
    <message>
        <source>New mission received, load datas from server</source>
        <translation type="vanished">Nouvelle mission reçues, chargement des données depuis le serveur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="323"/>
        <source>Connection error: </source>
        <translation>Erreur de connexion:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="329"/>
        <source>New version available: </source>
        <translation>Nouvelle version disponible: </translation>
    </message>
    <message>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation type="vanished">Arcade</translation>
    </message>
    <message>
        <source>Basic</source>
        <comment>profile</comment>
        <translation type="vanished">Basique</translation>
    </message>
    <message>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation type="vanished">Réaliste</translation>
    </message>
    <message>
        <source>Custom</source>
        <comment>profile</comment>
        <translation type="vanished">Perso</translation>
    </message>
    <message>
        <source>Mission</source>
        <comment>profile</comment>
        <translation type="vanished">Mission</translation>
    </message>
    <message>
        <source>Advance realistic</source>
        <comment>profile</comment>
        <translation type="vanished">Réaliste avancé</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="743"/>
        <source>Tactical View</source>
        <translation>Vue tactique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="827"/>
        <source>Connecting to %1:%2...</source>
        <comment>%1 will be replaced by server, %2 by port</comment>
        <translation>Connection à %1:%2</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="829"/>
        <source>Loading data...</source>
        <translation>Chargement des données...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="831"/>
        <source>Not connected - not registered</source>
        <translation>Non connecté - non enregistré</translation>
    </message>
    <message>
        <source>Advanced</source>
        <comment>profile</comment>
        <translation type="obsolete">Avancé</translation>
    </message>
    <message>
        <source>Off</source>
        <translation type="obsolete">Off</translation>
    </message>
    <message>
        <source>On</source>
        <translation type="obsolete">On</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="obsolete">Caché</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="obsolete">Affiché</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="obsolete">Rouge</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="obsolete">Bleue</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="obsolete">Clair</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="obsolete">Sombre</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Anglais</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="obsolete">Français</translation>
    </message>
    <message>
        <source>Deutsch</source>
        <translation type="obsolete">Allemand</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="obsolete">Italien</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="obsolete">Polonais</translation>
    </message>
    <message>
        <source>Czech</source>
        <translation type="obsolete">Tchèque</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="obsolete">Portuguais</translation>
    </message>
    <message>
        <source>Serbian</source>
        <translation type="obsolete">Serbe</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Espagnol</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="obsolete">Chinois</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="obsolete">Russe</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="obsolete">Suédois</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="832"/>
        <source>Not connected</source>
        <translation>Non connecté</translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation type="vanished">Connexion...</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="932"/>
        <source>Change &amp;unit</source>
        <translation>Changer d&apos;&amp;unité</translation>
    </message>
    <message>
        <source>Change unit</source>
        <translation type="vanished">Changer d&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="938"/>
        <source>Show circles</source>
        <translation>Afficher cercles</translation>
    </message>
    <message>
        <source>Show detection/threat circles</source>
        <translation type="vanished">Afficher/Cacher les cercles de détections/menaces</translation>
    </message>
    <message>
        <source>Circles on</source>
        <translation type="obsolete">Cercles on</translation>
    </message>
    <message>
        <source>Circles off</source>
        <translation type="obsolete">Cercles off</translation>
    </message>
    <message>
        <source>Show/Hide detection/threat circles</source>
        <translation type="obsolete">Afficher/Cacher les cercles de détections/menaces</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main.qml" line="944"/>
        <source>Show background label</source>
        <translation>Afficher le fond des étiquettes</translation>
    </message>
    <message>
        <source>Show/Hide background labels</source>
        <translation type="vanished">Afficher/cacher le fond des étiquettes</translation>
    </message>
    <message>
        <source>Disable fullscreen</source>
        <translation type="obsolete">Déactiver le plein écran</translation>
    </message>
    <message>
        <source>Enable fullscreen</source>
        <translation type="obsolete">Activer le plein écran</translation>
    </message>
    <message>
        <source>Show fullscreen</source>
        <translation type="vanished">Afficher en plein écran</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Connecter</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">Déconnecter</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <source>News</source>
        <translation type="obsolete">Nouvelles</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <source>Circle on</source>
        <translation type="obsolete">Cercles on</translation>
    </message>
    <message>
        <source>Circle off</source>
        <translation type="obsolete">Cercles off</translation>
    </message>
    <message>
        <source>METRIC</source>
        <translation type="obsolete">METRIQUE</translation>
    </message>
    <message>
        <source>IMPERIAL</source>
        <translation type="obsolete">IMPERIAL</translation>
    </message>
    <message>
        <source>Not registered&lt;br&gt;Put you %1 in the &lt;u&gt;%2 directory&lt;/u&gt;</source>
        <translation type="obsolete">Non enregistré&lt;br&gt;Mettre votre fichier %1 dans le répertoire &lt;u&gt;%2&lt;/u&gt;</translation>
    </message>
    <message>
        <source>If you do not have a license key &lt;u&gt;click HERE&lt;/u&gt; to buy one!</source>
        <translation type="obsolete">Si vous n&apos;avez pas de licence &lt;u&gt;Cliquer ici&lt;/u&gt; pour en acheter une!</translation>
    </message>
    <message>
        <source>Not registered</source>
        <translation type="obsolete">Non enregistré</translation>
    </message>
</context>
<context>
    <name>main_android</name>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="86"/>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="93"/>
        <source>Connection and authentification OK</source>
        <translation>Connexion et authentification OK</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="99"/>
        <source>Connection and authentification error: </source>
        <translation>Erreur de connexion ou d&apos;authentification:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="121"/>
        <source>New mission received, load datas from server</source>
        <translation>Nouvelle mission reçues, chargement des données depuis le serveur</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="137"/>
        <source>Connection error: </source>
        <translation>Erreur de connexion:</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="213"/>
        <source>Arcade</source>
        <comment>profile</comment>
        <translation>Arcade</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="214"/>
        <source>Basic</source>
        <comment>profile</comment>
        <translation>Basique</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="215"/>
        <source>Realistic</source>
        <comment>profile</comment>
        <translation>Réaliste</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="216"/>
        <source>Custom</source>
        <comment>profile</comment>
        <translation>Perso</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="311"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="316"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="321"/>
        <source>Weather</source>
        <translation>Météo</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="326"/>
        <source>BRAA</source>
        <translation>BRAA</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="331"/>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="336"/>
        <source>Glide</source>
        <translation>Glide</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="341"/>
        <source>Draw</source>
        <translation>Dessin</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="348"/>
        <source>Airport</source>
        <translation>Aéroport</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="361"/>
        <source>SITAC</source>
        <translation>SITAC</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="408"/>
        <source>Not connected</source>
        <translation>Non connecté</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="578"/>
        <source>Change &amp;unit</source>
        <translation>Changer d&apos;&amp;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="581"/>
        <source>Change unit</source>
        <translation>Changer d&apos;unité</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="585"/>
        <source>Show circles</source>
        <translation>Afficher cercles</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="588"/>
        <source>Show detection/threat circles</source>
        <translation>Afficher/Cacher les cercles de détections/menaces</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="592"/>
        <source>Show background label</source>
        <translation>Afficher le fond des étiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="595"/>
        <source>Show/Hide background labels</source>
        <translation>Afficher/cacher le fond des étiquettes</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Disable fullscreen</source>
        <translation>Déactiver le plein écran</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="600"/>
        <source>Enable fullscreen</source>
        <translation>Activer le plein écran</translation>
    </message>
    <message>
        <location filename="../lotatc/client/resources/qml/client/main_android.qml" line="614"/>
        <source>Show fullscreen</source>
        <translation>Afficher en plein écran</translation>
    </message>
</context>
</TS>
